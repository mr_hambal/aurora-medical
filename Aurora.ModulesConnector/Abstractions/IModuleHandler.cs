﻿using System.Collections.Generic;

namespace Aurora.ModulesConnector
{
    public interface IModuleHandler
    {
        void Search();
        IList<AbstractModule> GetList();
        void PlugIn(AbstractModule moduleUnit);
        void PlugOut(AbstractModule moduleUnit);
    }
}