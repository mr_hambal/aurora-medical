﻿namespace Aurora.ModulesConnector
{
    public enum ModuleStateEnum
    {
        NotActivated = 1,
        Activated = 2,
        Working = 3,
        Error = 9
    }
    
}