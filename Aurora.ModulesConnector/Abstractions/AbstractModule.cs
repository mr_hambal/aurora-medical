﻿using System;

namespace Aurora.ModulesConnector
{
    /// <summary>
    /// Абстрактный класс, представляющий структуру модулей приложения.
    /// </summary>
    public abstract class AbstractModule
    {
        private readonly ModuleStateEnum _state;
        private readonly String _name;

        protected AbstractModule()
        {
            _state = ModuleStateEnum.NotActivated;
            _name = "NewModuleName";
        }

        public String ModuleName()
        {
            return _name;
        }

#region Overrideable methods

        public virtual void Activate()
        {
            throw new NotImplementedException();
        }

        public virtual ModuleStateEnum GetState()
        {
            return _state;
        }

        public virtual void Run()
        {
            throw new NotImplementedException();
        }

        public virtual void DeActivate()
        {
            throw new NotImplementedException();
        }

#endregion
    }
}