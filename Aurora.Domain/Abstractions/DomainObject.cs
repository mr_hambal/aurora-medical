﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    public abstract class DomainObject : IEquatable<DomainObject>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; protected set; }

        public bool IsNull = true;

        #region Implementation of IEquatable<DomainObject>

        /// <summary>
        /// Указывает, равен ли текущий объект другому объекту того же типа.
        /// </summary>
        /// <returns>
        /// true, если текущий объект равен параметру <paramref name="other"/>, в противном случае — false.
        /// </returns>
        /// <param name="other">Объект, который требуется сравнить с данным объектом.</param>
        public abstract bool Equals(DomainObject other);

        /// <summary>
        /// Заставляем все наследующие классы реализовать возврат хэш-кода
        /// </summary>
        /// <returns>int: значение хэш-кода</returns>
        public abstract int GetHashCode();

        /// <summary>
        /// Переопределение идентификатора
        /// </summary>
        /// <param name="newId">Идентификатор</param>
        public void SetId(long newId)
        {
            Id = newId;
        }

        #endregion
    }
}