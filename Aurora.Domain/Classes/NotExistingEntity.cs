using System;

namespace Aurora.Domain
{
    public class NotExistingEntity : DomainObject
    {
        public NotExistingEntity()
        {
            IsNull = true;
        }

        public override bool Equals(DomainObject other)
        {
            return GetType() == other.GetType();
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}