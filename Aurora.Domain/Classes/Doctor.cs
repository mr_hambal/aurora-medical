﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    /// <summary>
    /// Класс-сущность 
    /// </summary>
    public class Doctor : DomainObject, IEquatable<Doctor>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        [Required(ErrorMessage = "Не указано имя сотрудника")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указана должность")]
        public string Post { get; set; }

        [Required(ErrorMessage = "Укажите краткую информацию о сотруднике")]
        public string Info { get; set; }

        /// <summary>
        /// Скрытый беспараметрический конструктор.
        /// </summary>
        private Doctor()
        {
            IsNull = false;
        }

        /// <summary>
        /// Фабричный метод
        /// </summary>
        /// <param name="name">Имя доктора</param>
        /// <param name="post">Должность</param>
        /// <param name="info">Краткая информация</param>
        /// <returns>Doctor</returns>
        public static Doctor Create(string name, string post, string info)
        {
            return new Doctor()
                       {
                           IsNull = false,
                           Name = name,
                           Post = post,
                           Info = info
                       };
        }

        public override string ToString()
        {
            return Name;
        }

        #region IEquatable implementation
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() && Equals((Doctor)other);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Post.GetHashCode() + Info.GetHashCode();
        }

        public bool Equals(Doctor other)
        {
            return (
                        Name.ToLower() == other.Name.ToLower() &&
                        Info.ToLower() == other.Info.ToLower() &&
                        Post.ToLower() == other.Post.ToLower()
                    );
        }

        public static bool operator ==(Doctor lhs, Doctor rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Doctor lhs, Doctor rhs)
        {
            return !(lhs == rhs);
        }
        #endregion
    }
}