using System;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    /// <summary>
    /// �������� ������ �����������, ����������� � ������-���� ������������ �����������
    /// </summary>
    
    public class Disease : DomainObject, IEquatable<Disease>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        [Required(ErrorMessage = "������� �������� �������!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "���������� �������� ������� ����������!")]
        public string Info { get; set; }

        [Required(ErrorMessage = "���������� �������� �������� ���������!")]
        public string Cathegory { get; set; }

        /// <summary>
        /// ������� ������������������ �����������.
        /// </summary>
        private Disease()
        {
            IsNull = false;
        }

        /// <summary>
        /// ��������� �����
        /// </summary>
        /// <param name="name">�������� �������</param>
        /// <param name="info">������� ����������</param>
        /// <param name="cathegory">������ ��������</param>
        /// <returns></returns>
        public static Disease Create(string name, string info, string cathegory)
        {
            return new Disease() {Name = name, Info = info, Cathegory = cathegory, IsNull = false};
        }

        public override string ToString()
        {
            return Name;
        } 

        #region IEquatable implementation
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() && Equals((Disease)other);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Info.GetHashCode() + Cathegory.GetHashCode();
        }

        public bool Equals(Disease other)
        {
            return (
                        Name.ToLower() == other.Name.ToLower() &&
                        Info.ToLower() == other.Info.ToLower() &&
                        Cathegory.ToLower() == other.Cathegory.ToLower()
                    );
        }

        public static bool operator ==(Disease lhs, Disease rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Disease lhs, Disease rhs)
        {
            return !(lhs == rhs);
        }
        #endregion
    }
}