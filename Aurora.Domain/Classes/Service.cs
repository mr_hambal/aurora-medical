using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    public class Service : DomainObject, IEquatable<Service>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        [Required(ErrorMessage = "������� �������� ������!")]
        public string Name { get; private set; }

        public string Info { get; private set; }

        [Required(ErrorMessage = "������� ���� ������!")]
        public double Price { get; private set; }

        public virtual ICollection<Visit> Visits { get; set; }

        /// <summary>
        /// ������������������ �����������.
        /// </summary>
        private Service()
        {
            IsNull = false;
            Visits = new List<Visit>();
        }

        /// <summary>
        /// ��������� �����
        /// </summary>
        /// <param name="name">�������� ������</param>
        /// <param name="info">������� ��������</param>
        /// <param name="price">��������� ������</param>
        public static Service Create(string name, string info, double price)
        {
            return new Service() {Name = name, Info = info, Price = price};
        }

        public override string ToString()
        {
            return Name;
        }

        #region Implementation of IEquatable

        /// <summary>
        /// ����������, ����� �� �������� ������ <see cref="T:System.Object"/> �������� ������� <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// �������� true, ���� �������� ������ <see cref="T:System.Object"/> ����� �������� ������� <see cref="T:System.Object"/>; � ��������� ������ � �������� false.
        /// </returns>
        /// <param name="obj">������� <see cref="T:System.Object"/>, ������� ��������� �������� � ������� ��������� <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object other)
        {
            return other.GetType() == GetType() ? Equals((Service)other) : false;
        }

        /// <summary>
        /// ���������, ����� �� ������� ������ ������� ������� ���� �� ����.
        /// </summary>
        /// <returns>
        /// true, ���� ������� ������ ����� ��������� <paramref name="other"/>, � ��������� �����堗 false.
        /// </returns>
        /// <param name="other">������, ������� ��������� �������� � ������ ��������.</param>
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() ? Equals((Service) other) : false;
        }

        public bool Equals(Service other)
        {
            return (Name == other.Name && Info == other.Info);
        }

        /// <summary>
        /// ���������� ��� ����������� ������ ����������� ������� ���-����
        /// </summary>
        /// <returns>int: �������� ���-����</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() + Info.GetHashCode();
        }

        public static bool operator ==(Service lhs, Service rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Service lhs, Service rhs)
        {
            return !(lhs == rhs);
        }

        #endregion
    }
}