using System;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    public class Diagnosis : DomainObject, IEquatable<Diagnosis>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        /// <summary>
        /// �������
        /// </summary>
        [Required(ErrorMessage = "������� �������")]
        public Disease DiagnosisDisease { get; set; }

        /// <summary>
        /// ������� ����
        /// </summary>
        [Required(ErrorMessage = "������� �������")]
        public string DiagnosisInfo { get; set; }

        /// <summary>
        /// ������������������ �����������.
        /// </summary>
        private Diagnosis()
        {
            IsNull = false;
        }

        /// <summary>
        /// ��������� �����
        /// </summary>
        /// <param name="disease">�����������</param>
        /// <param name="info">�������</param>
        /// <returns>Visit</returns>
        public static Diagnosis Create(Disease disease, String info)
        {
            return new Diagnosis()
                       {
                           DiagnosisDisease = disease, 
                           DiagnosisInfo = info
                       };
        }

        public override string ToString()
        {
            return DiagnosisDisease.Name;
        }

#region implementations of IEquatable
        /// <summary>
        /// ����������, ����� �� �������� ������ <see cref="T:System.Object"/> �������� ������� <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// �������� true, ���� �������� ������ <see cref="T:System.Object"/> ����� �������� ������� <see cref="T:System.Object"/>; � ��������� ������ � �������� false.
        /// </returns>
        /// <param name="other">������� <see cref="T:System.Object"/>, ������� ��������� �������� � ������� ��������� <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object other)
        {
            return other.GetType() == GetType() && Equals((Diagnosis)other);
        }

        /// <summary>
        /// ���������, ����� �� ������� ������ ������� ������� ���� �� ����.
        /// </summary>
        /// <returns>
        /// true, ���� ������� ������ ����� ��������� <paramref name="other"/>, � ��������� �����堗 false.
        /// </returns>
        /// <param name="other">������, ������� ��������� �������� � ������ ��������.</param>
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() && Equals((Diagnosis)other);
        }

        /// <summary>
        /// ���������� ��� ����������� ������ ����������� ������� ���-����
        /// </summary>
        /// <returns>int: �������� ���-����</returns>
        public override int GetHashCode()
        {
            return DiagnosisInfo.GetHashCode() + DiagnosisDisease.GetHashCode();
        }

        public bool Equals(Diagnosis other)
        {
            return (
                        DiagnosisDisease == other.DiagnosisDisease &&
                        DiagnosisInfo == other.DiagnosisInfo
                    );
        }

        public static bool operator ==(Diagnosis lhs, Diagnosis rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Diagnosis lhs, Diagnosis rhs)
        {
            return !(lhs == rhs);
        }
    }
#endregion
}