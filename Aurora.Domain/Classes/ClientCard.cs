using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    public class ClientCard: DomainObject, IDisposable, IEquatable<ClientCard>
    {
        private bool _disposed;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        [Required(ErrorMessage = "������� ��� ��������!")]
        public string FirstName { get; private set; }

        [Required(ErrorMessage = "������� �������� ��������!")]
        public string Initials { get; private set; }

        [Required(ErrorMessage = "������� ������� ��������!")]
        public string LastName { get; private set; }

        [Required(ErrorMessage = "������� ���� �������� ��������!")]
        public DateTime BirthDay { get; private set; }

        [Required(ErrorMessage = "������� ����� ��������!")]
        public string Address { get; private set; }

        public virtual ICollection<Visit> Visits { get; set; }
        
        /// <summary>
        /// Returns new empty ClientCard
        /// </summary>
        private ClientCard()
        {
            IsNull = false;
            Visits = new List<Visit>();
        }

        /// <summary>
        /// ��������� �����  
        /// </summary>
        /// <param name="firstName">��� �������</param>
        /// <param name="initials">��������</param>
        /// <param name="lastName">������� �������</param>
        /// <param name="birthDay">���� ��������</param>
        /// <param name="address">����� ����������</param>
        /// <param name="visits">������ ���������. �� ��������� �� ����������, � ���������������� �������������. ���������� ������ ��� ����� ������������ �����.</param>
        /// <returns>���������� ����� ��������� ������</returns>
        public static ClientCard Create(string firstName, string initials, string lastName, DateTime birthDay, string address, ICollection<Visit> visits = null)
        {
            return new ClientCard()
                       {
                           FirstName = firstName,
                           Initials = initials,
                           LastName = lastName,
                           BirthDay = birthDay,
                           Address = address
                       };
        }

        /// <summary>
        /// ���������� ������ ��� ������� � ������� "������� ���"
        /// </summary>
        /// <returns>������� ��� ��������</returns>
        public String GetFullName()
        {
            return String.Format("{0} {1}", LastName, FirstName);
        }

        public override string ToString()
        {
            return GetFullName();
        }
        
    #region Implementation of ICloneable

        public ClientCard Clone(ClientCard other)
        {
            return Create(other.FirstName, other.LastName, other.Initials, other.BirthDay, other.Address, Visits = other.Visits);
        }

    #endregion

    #region Implementation of IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Visits.Clear();
                    Visits = null;
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                _disposed = true;
            }
        }

    #endregion

        #region Implementation of IEquatable

        /// <summary>
        /// ����������, ����� �� �������� ������ <see cref="T:System.Object"/> �������� ������� <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// �������� true, ���� �������� ������ <see cref="T:System.Object"/> ����� �������� ������� <see cref="T:System.Object"/>; � ��������� ������ � �������� false.
        /// </returns>
        /// <param name="other">������� <see cref="T:System.Object"/>, ������� ��������� �������� � ������� ��������� <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object other)
        {
            return other.GetType() == GetType() && Equals((ClientCard)other);
        }

        /// <summary>
        /// ���������, ����� �� ������� ������ ������� ������� ���� �� ����.
        /// </summary>
        /// <returns>
        /// true, ���� ������� ������ ����� ��������� <paramref name="other"/>, � ��������� �����堗 false.
        /// </returns>
        /// <param name="other">������, ������� ��������� �������� � ������ ��������.</param>
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() && Equals((ClientCard) other);
        }

        /// <summary>
        /// ���������� ��� ����������� ������ ����������� ������� ���-����
        /// </summary>
        /// <returns>int: �������� ���-����</returns>
        public override int GetHashCode()
        {
            return FirstName.GetHashCode() + Initials.GetHashCode() + LastName.GetHashCode();
        }

        /// <summary>
        /// ���������, ����� �� ������� ������ ������� ������� ���� �� ����.
        /// </summary>
        /// <returns>
        /// true, ���� ������� ������ ����� ��������� <paramref name="other"/>, � ��������� �����堗 false.
        /// </returns>
        /// <param name="other">������, ������� ��������� �������� � ������ ��������.</param>
        public bool Equals(ClientCard other)
        {
            return (
                        FirstName.ToLower() == other.FirstName.ToLower() && 
                        Initials.ToLower() == other.Initials.ToLower() && 
                        LastName.ToLower() == other.LastName.ToLower() &&
                        BirthDay.ToLocalTime() == other.BirthDay.ToLocalTime()
                    );
        }

        public static bool operator ==(ClientCard lhs, ClientCard rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ClientCard lhs, ClientCard rhs)
        {
            return !(lhs == rhs);
        }
        #endregion
    }
}