using System.Data.Entity;

namespace Aurora.Domain
{
    public class AuroraEntities : DbContext
    {
        /// <summary>
        /// Constructs a new context instance using conventions to create the name of the database to
        ///             which a connection will be made.  The by-convention name is the full name (namespace + class name)
        ///             of the derived context class.
        ///             See the class remarks for how this is used to create a connection.
        /// </summary>
        public AuroraEntities()
        {
            Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        /// ������ ���� ���������
        /// </summary>
        public DbSet<ClientCard> ClientCards { get; set; }

        /// <summary>
        /// ������ ��������
        /// </summary>
        public DbSet<Disease> Diseases { get; set; }

        /// <summary>
        /// ������ ���������
        /// </summary>
        public DbSet<Diagnosis> Diagnoses { get; set; }

        /// <summary>
        /// ������ �����������
        /// </summary>
        public DbSet<Doctor> Doctors { get; set; }

        /// <summary>
        /// ������ �����
        /// </summary>
        public DbSet<Service> Services { get; set; }
        
        /// <summary>
        /// �������� ���������
        /// </summary>
        public DbSet<Visit> Visits { get; set; }
    }
}