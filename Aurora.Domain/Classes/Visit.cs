using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Aurora.Domain
{
    public class Visit : DomainObject, IEquatable<Visit>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; protected set; }

        /// <summary>
        /// �������
        /// </summary>
        public virtual ICollection<Diagnosis> VisitDiagnosis { get; set; }

        /// <summary>
        /// ������� ����
        /// </summary>
        [Required(ErrorMessage = "������� �������� �����")]
        public virtual Doctor VisitDoctor { get; set; }

        /// <summary>
        /// ���� ���������
        /// </summary>
        [Required(ErrorMessage = "������� ���� ���������")]
        public DateTime VisitDate { get; set; }

        /// <summary>
        /// ����� ��������
        /// </summary>
        [Required(ErrorMessage = "�� ������ �������")]
        public virtual ClientCard RelatedCard { get; set; }

        /// <summary>
        /// ������ ��������� �����
        /// </summary>
        [Required(ErrorMessage = "�� ������� �� ����� ������")]
        public virtual ICollection<Service> Services { get; set; }
        
        /// <summary>
        /// ������������������ �����������.
        /// </summary>
        private Visit()
        {
            IsNull = false;
            Services = new List<Service>();
        }

        /// <summary>
        /// ��������� �����
        /// </summary>
        /// <param name="diagnosis">�������</param>
        /// <param name="doctor">������</param>
        /// <param name="dateTime">����� ������</param>
        /// <param name="relatedCard">�������</param>
        /// <returns>Visit</returns>
        public static Visit Create(ICollection<Diagnosis> diagnosis, Doctor doctor, DateTime dateTime, ClientCard relatedCard)
        {
            return new Visit()
                       {
                           IsNull = false,
                           VisitDate = dateTime,
                           VisitDiagnosis = diagnosis, 
                           VisitDoctor = doctor,
                           RelatedCard = relatedCard
                       };
        }

        /// <summary>
        /// ����������, ����� �� �������� ������ <see cref="T:System.Object"/> �������� ������� <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// �������� true, ���� �������� ������ <see cref="T:System.Object"/> ����� �������� ������� <see cref="T:System.Object"/>; � ��������� ������ � �������� false.
        /// </returns>
        /// <param name="other">������� <see cref="T:System.Object"/>, ������� ��������� �������� � ������� ��������� <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object other)
        {
            return other.GetType() == GetType() && Equals((Visit)other);
        }

        /// <summary>
        /// ���������, ����� �� ������� ������ ������� ������� ���� �� ����.
        /// </summary>
        /// <returns>
        /// true, ���� ������� ������ ����� ��������� <paramref name="other"/>, � ��������� �����堗 false.
        /// </returns>
        /// <param name="other">������, ������� ��������� �������� � ������ ��������.</param>
        public override bool Equals(DomainObject other)
        {
            return other.GetType() == GetType() && Equals((Visit)other);
        }

        /// <summary>
        /// ���������� ��� ����������� ������ ����������� ������� ���-����
        /// </summary>
        /// <returns>int: �������� ���-����</returns>
        public override int GetHashCode()
        {
            return VisitDate.GetHashCode() + VisitDiagnosis.GetHashCode();
        }

        public bool Equals(Visit other)
        {
            return (
                        VisitDiagnosis == other.VisitDiagnosis &&
                        VisitDoctor == other.VisitDoctor &&
                        RelatedCard == other.RelatedCard &&
                        VisitDate.ToLocalTime() == other.VisitDate.ToLocalTime()
                    );
        }

        public static bool operator ==(Visit lhs, Visit rhs)
        {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Visit lhs, Visit rhs)
        {
            return !(lhs == rhs);
        }
    }
}