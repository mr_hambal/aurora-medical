﻿using System;
using Aurora.Infrastructure;
using Microsoft.Win32;

namespace Aurora.WinRegistry
{
    public class PathHandler : IPathHandler
    {
        private RegistryKey _registryKey;

        /// <summary>
        /// Возвращает полный абсолютный путь к установленной программе.
        /// </summary>
        /// <returns>
        /// Путь в виде строкового значения
        /// </returns>
        public string GetPath()
        {
            if (FindOrCreateKey() == false) return String.Empty;

            var path = String.Empty;

            try
            {
                path = _registryKey.GetValue("InstallPath").ToString();
            }
            catch (Exception)
            {
                Messenger.ShowErrorMessage("Ошибка при чтении из реестра",
                                           "Проверьте наличие необходимых прав доступа.");
            }

            return path;
        }

        /// <summary>
        /// Записывает путь к установленной программе в HKCU\Software\Selfnamed\Aurora.
        /// </summary>
        /// <param name="path">
        /// Полный абсолютный путь к программе.
        /// </param>
        public void SetPath(string path)
        {
            if (FindOrCreateKey() == false) return;

            try
            {
                _registryKey.SetValue("InstallPath", path, RegistryValueKind.String);
            }
            catch (Exception exception)
            {
                Messenger.ShowErrorMessage("Ошибка при записи в реестр",
                                           "Проверьте наличие необходимых прав доступа." + exception.Message);
                return;
            }
        }

        /// <summary>
        /// Проверяет наличие ключа в реестре и, при необходимости, создает его.
        /// </summary>
        /// <returns>
        /// Возвращет логическое значение существования ключа в реестре или возможности доступа к нему.
        /// </returns>
        public bool FindOrCreateKey()
        {
            _registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Selfnamed\\Aurora", true);

            if (_registryKey == null)
            {
                try
                {
                    _registryKey = Microsoft.Win32.Registry.CurrentUser
                        .OpenSubKey("Software", true)
                        .CreateSubKey("Selfnamed", RegistryKeyPermissionCheck.ReadWriteSubTree)
                        .CreateSubKey("Aurora", RegistryKeyPermissionCheck.ReadWriteSubTree);
                }
                catch (Exception)
                {
                    Messenger.ShowErrorMessage("Ошибка при обращении к реестру",
                                           "Проверьте наличие необходимых прав доступа.");
                    _registryKey.Close();

                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Удаляет все упоминания о программе из реестра.
        /// </summary>
        public void Clear()
        {
            try
            {
                Microsoft.Win32.Registry.CurrentUser.OpenSubKey("\\Software\\Selfnamed").DeleteSubKeyTree("Aurora");
            }
            catch (Exception exception)
            {
                Messenger.ShowErrorMessage("Ошибка при обращении к реестру",
                                           "Отсутствует ключ или необходимые права доступа." + exception.Message);
            }
        }
    }
}
