﻿namespace Aurora.WinRegistry
{
    /// <summary>
    /// Предоставляет методы добавления/чтения/удаления ключей, связанных с программой, в системный реестр.
    /// </summary>
    public interface IPathHandler
    {
        bool FindOrCreateKey();
        string GetPath();
        void SetPath(string path);
        void Clear();
    }
}