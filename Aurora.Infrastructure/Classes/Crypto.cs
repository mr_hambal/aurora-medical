using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Aurora.Infrastructure
{
    public static class Crypto
    {
        private static RSACryptoServiceProvider _rsa;

        private static void RsaAssignParameter()
        {
            const int providerRsaFull = 1;
            const string containerName = "SpiderContainer";
            var cspParams = new CspParameters(providerRsaFull)
                                {
                                    KeyContainerName = containerName,
                                    Flags = CspProviderFlags.UseMachineKeyStore,
                                    ProviderName = "Microsoft Strong Cryptographic Provider"
                                };
            _rsa = new RSACryptoServiceProvider(cspParams);
        }

        public static string RsaEncryptData(string data2Encrypt)
        {
            RsaAssignParameter();
            var reader = new StreamReader(@"C:\publickey.xml");
            var publicOnlyKeyXml = reader.ReadToEnd();
            _rsa.FromXmlString(publicOnlyKeyXml);
            reader.Close();

            //read plaintext, encrypt it to ciphertext

            var plainbytes = Encoding.UTF8.GetBytes(data2Encrypt);
            var cipherbytes = _rsa.Encrypt(plainbytes, false);
            return Convert.ToBase64String(cipherbytes);
        }

        public static void RsaAssignNewKey()
        {
            RsaAssignParameter();

            //provide public and private RSA params
            var writer = new StreamWriter(@"C:\privatekey.xml");
            var publicPrivateKeyXml = _rsa.ToXmlString(true);
            writer.Write(publicPrivateKeyXml);
            writer.Close();

            //provide public only RSA params
            writer = new StreamWriter(@"C:\publickey.xml");
            var publicOnlyKeyXml = _rsa.ToXmlString(false);
            writer.Write(publicOnlyKeyXml);
            writer.Close();
        }

        public static string RsaDecryptData(string data2Decrypt)
        {
            RsaAssignParameter();

            var getpassword = Convert.FromBase64String(data2Decrypt);

            var reader = new StreamReader(@"C:\privatekey.xml");
            var publicPrivateKeyXml = reader.ReadToEnd();
            _rsa.FromXmlString(publicPrivateKeyXml);
            reader.Close();

            //read ciphertext, decrypt it to plaintext
            var plain = _rsa.Decrypt(getpassword, false);
            return Encoding.UTF8.GetString(plain);
        }

        public static string GetMd5Hash(SecureString input)
        {
            var hashProvider = new MD5CryptoServiceProvider();
            var bytes = Encoding.UTF8.GetBytes(input.ToString());
            var s = new StringBuilder();
            
            bytes = hashProvider.ComputeHash(bytes);

            foreach (var byt in bytes)
            {
                s.Append(byt.ToString("x2").ToLower());
            }

            return s.ToString();
        }
    }
}