using System;
using System.Text.RegularExpressions;

namespace Aurora.Infrastructure
{
    public static class TextProcessor
    {
        public static string RusChar(string text)
        {
            return Regex.Replace(text, @"[^�-� -']", String.Empty);
        }

        public static Boolean IpMatches(string text)
        {
            return Regex.Match(text, @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$").Success;
        }

        public static String PriceChar(String text)
        {
            return Regex.Replace(text, @"[^\d.']", String.Empty);
        }
    }
}