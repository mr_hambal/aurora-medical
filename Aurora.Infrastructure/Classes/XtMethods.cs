﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aurora.Infrastructure
{
    /// <summary>
    /// Класс, содержащий методы расширений.
    /// </summary>
    public static class XtMethods
    {
        /// <summary>
        /// Метод возвращает сам объект или null, если объект не существует.
        /// </summary>
        public static TResult With<TInput, TResult>(this TInput o, Func<TInput, TResult> evaluator)
            where TResult : class
            where TInput : class
        {
            return o == null 
                ? null 
                : evaluator(o);
        }

        /// <summary>
        /// Метод возвращает сам объект или заданное значение failureValue, если объект не существует.
        /// </summary>
        public static TResult Return<TInput, TResult>(this TInput o, Func<TInput, TResult> evaluator, TResult failureValue) 
            where TInput : class
        {
            return o == null 
                ? failureValue 
                : evaluator(o);
        }

        public static IList<T> Intersection<T>(this IEnumerable<T> o, IEnumerable<T> other) where T:IEquatable<T>
        {
            return (from item in o
                    from item2 in other
                    where item.Equals(item2)
                    select item)
                .ToList();
        }
    }
}