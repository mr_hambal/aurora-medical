using System.Windows.Forms;

namespace Aurora.Infrastructure
{
    public static class Messenger
    {
        public static void ShowErrorMessage(string errorHeader, string errorDescr)
        {
            MessageBox.Show(errorDescr, errorHeader, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult ShowAcceptDeclineChangesQuestion()
        {
            var msgBoxResult = MessageBox.Show(
                                                   "��������� ��� ���������?",
                                                   "��������!",
                                                   MessageBoxButtons.YesNoCancel,
                                                   MessageBoxIcon.Question,
                                                   MessageBoxDefaultButton.Button1
                                               );
            return msgBoxResult;
        }

        public static DialogResult ShowYesNoQuestion(string question)
        {
            var msgBoxResult = MessageBox.Show(
                                                   question,
                                                   "��������!",
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Question,
                                                   MessageBoxDefaultButton.Button1
                                               );
            return msgBoxResult;
        }

        public static void ShowExclamation(string exclamation)
        {
            MessageBox.Show(
                                exclamation,
                                "��������!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1
                            );
            return;
        }
    }
}