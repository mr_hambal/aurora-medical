using System;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using Aurora.Infrastructure;
using MySql.Data.MySqlClient;

namespace Aurora.DataAccess
{
    public class DbSelector
    {
        public String GetMySqlConnStr(string addr, string name, string pass)
        {
            var mySqlConnectionStringBuilder = new MySqlConnectionStringBuilder
            {
                Database = "Aurora",
                Server = addr,
                UserID = name,
                Password = pass
            };

            return mySqlConnectionStringBuilder.ConnectionString;
        }

        public String GetMsSqlConnStr(string addr, string name, string pass)
        {
            var msSqlConnectionStringBuilder = new SqlConnectionStringBuilder()
            {
                InitialCatalog = "Aurora",
                DataSource = addr,
                UserID = name,
                Password = pass,
                ConnectTimeout = 30
            };

            return msSqlConnectionStringBuilder.ConnectionString;
        }

        public String GetSqlCeConnStr()
        {
            var sqlCeConnectionStringBuilder = new SqlCeConnectionStringBuilder(@"Data Source=|DataDirectory|\Aurora.sdf; Persist Security Info=False");

            return sqlCeConnectionStringBuilder.ConnectionString;
        }

        public String GetMySqlProvName()
        {
            return "MySql.Data.MySqlClient";
        }

        public String GetMsSqlProvName()
        {
            return "System.Data.SqlClient";
        }

        public String GetSqlCeProvName()
        {
            return "System.Data.SqlServerCe.4.0";
        }

        public Boolean CheckConnection(string provName, string connStr)
        {
            IDbConnection dbConnection;

            switch (provName)
            {
                case "System.Data.SqlServerCe.4.0":
                    {
                        dbConnection = new SqlCeConnection(connStr);
                        break;
                    }
                case "MySql.Data.MySqlClient":
                    {
                        dbConnection = new MySqlConnection(connStr);
                        break;
                    }
                case "System.Data.SqlClient":
                    {
                        dbConnection = new SqlConnection(connStr);
                        break;
                    }
                default:
                    {
                        throw new ProviderException("����������� ��� �������� ���������������� ��������� �� ��������� ��. ��������: " + provName);
                    }
            }

            try
            {
                dbConnection.Open();
                dbConnection.Close();
                return true;
            }
            catch (Exception e)
            {
                Messenger.ShowErrorMessage("���������� ����������� � ���� ������.", "�������� ��������: " + e.Message);
                return false;
            }
        }
    }
}