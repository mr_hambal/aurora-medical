﻿using System;

namespace Aurora.DataAccess
{
    public interface IDbValidator: IDisposable
    {
        bool IsDbExist();
        bool CreateDb();
        bool CheckModel();
        void InitDb();
        bool MergeDb();
    }
}