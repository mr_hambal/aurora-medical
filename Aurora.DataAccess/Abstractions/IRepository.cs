﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using Aurora.Domain;

namespace Aurora.DataAccess
{
	/// <summary>
	/// IClientCardRepository - interface for DB Repositories. Encapsulate work with Entities in DB.
	/// </summary>
	public interface IRepository
	{
	    DbContext GetContext();
        IList<T> GetObjects<T>() where T:DomainObject;
	    IList GetObjects(Type domainType);
        IList GetObjects(Func<DomainObject, DomainObject> query);
        T GetObject<T>(long id) where T : DomainObject;
        bool Update(DomainObject origEntity, DomainObject entity);
        bool Insert(DomainObject entity);
        bool Remove(DomainObject entity);
		bool SubmitChanges();
	    bool RejectChanges();
	    void Dispose();
	}
}
