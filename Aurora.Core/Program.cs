﻿using System;
using System.Windows.Forms;
using Aurora.Core.ViewForms;
using Aurora.DataAccess;
using Aurora.Infrastructure.IOC;
using Microsoft.Practices.Unity;

namespace Aurora.Core
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var unityContainer = IocFabric.CreateConfigured("workingModel");
            var frmConnect = new frmConnect();

            frmConnect.ShowDialog();
            
            using (var dbValidator = unityContainer.Resolve<IDbValidator>())
            {
                dbValidator.InitDb();
            }

            Application.Run(new frmMain(unityContainer));
        }
    }
}
