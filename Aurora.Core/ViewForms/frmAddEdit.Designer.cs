﻿using Binarymission.WinFormControls.TabControls;

namespace Aurora.Core.ViewForms
{
    partial class frmAddEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.kryptonManager1 = new ComponentFactory.Krypton.Toolkit.KryptonManager(this.components);
            this.grboxAddNew = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.tabMainAdd = new Binarymission.WinFormControls.TabControls.BinaryPowerTabStrip();
            this.tabCard = new Binarymission.WinFormControls.TabControls.BinaryPowerTabPage();
            this.btnCardAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tbAddressAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblAddressAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.dtBirthDayAdd = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.lblBirthDayAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbInitialsAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbFirstNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblInitialsAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblFirstNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbLastNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblLastNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabDisease = new Binarymission.WinFormControls.TabControls.BinaryPowerTabPage();
            this.btnDiseaseAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tbDiseaseInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbDiseaseCathegoryAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbDiseaseNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblDiseaseInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblDiseaseCathegoryAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblDiseaseNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabDoctor = new Binarymission.WinFormControls.TabControls.BinaryPowerTabPage();
            this.tbDoctorInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbDoctorPostAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbDoctorNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblDoctorInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblDoctorPostAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblDoctorNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btnDoctorAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tabService = new Binarymission.WinFormControls.TabControls.BinaryPowerTabPage();
            this.btnSeviceAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tbServiceInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbServicePriceAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbServiceNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblServiceInfoAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblServicePriceAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblServiceNameAdd = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btnCancel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.validationWarningToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnDone = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.grboxAddNew)).BeginInit();
            this.grboxAddNew.Panel.SuspendLayout();
            this.grboxAddNew.SuspendLayout();
            this.tabMainAdd.SuspendLayout();
            this.tabCard.SuspendLayout();
            this.tabDisease.SuspendLayout();
            this.tabDoctor.SuspendLayout();
            this.tabService.SuspendLayout();
            this.SuspendLayout();
            // 
            // grboxAddNew
            // 
            this.grboxAddNew.Location = new System.Drawing.Point(13, 13);
            this.grboxAddNew.Margin = new System.Windows.Forms.Padding(10);
            this.grboxAddNew.Name = "grboxAddNew";
            // 
            // grboxAddNew.Panel
            // 
            this.grboxAddNew.Panel.Controls.Add(this.tabMainAdd);
            this.grboxAddNew.Size = new System.Drawing.Size(447, 379);
            this.grboxAddNew.TabIndex = 0;
            this.grboxAddNew.Values.Heading = "Добавление нового элемента";
            // 
            // tabMainAdd
            // 
            this.tabMainAdd.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabMainAdd.CustomEndColor = System.Drawing.Color.White;
            this.tabMainAdd.CustomStartColor = System.Drawing.Color.Green;
            this.tabMainAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMainAdd.DrawBorderAroundTabStrip = false;
            this.tabMainAdd.DrawTabPageOnTopOfEachOther = false;
            this.tabMainAdd.EmptySpaceLengthBeforeTheFirstTabPage = 8;
            this.tabMainAdd.EnableAutomaticUpdateOfTabPagesHeaderSize = false;
            this.tabMainAdd.EnableControlledLayoutRendering = false;
            this.tabMainAdd.EnableDragAndDropOfTabPages = false;
            this.tabMainAdd.EnableTabPageLevelCloseButtonRendering = false;
            this.tabMainAdd.ExtendedWindowsXPRenderingFirstColor = System.Drawing.Color.Blue;
            this.tabMainAdd.ExtendedWindowsXPRenderingSecondColor = System.Drawing.Color.LightBlue;
            this.tabMainAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabMainAdd.IsBackgroundTransparant = false;
            this.tabMainAdd.KeepShowingTooltipEvenWhenMousePointerIsMoving = false;
            this.tabMainAdd.Location = new System.Drawing.Point(0, 0);
            this.tabMainAdd.Name = "tabMainAdd";
            this.tabMainAdd.NewlyAddedPagesAreLocatedAtLeftCorner = false;
            this.tabMainAdd.PreventInvalidation = false;
            this.tabMainAdd.RenderFullTextForTabPageHeader = false;
            this.tabMainAdd.SelectedPageHeaderTextIsRenderedBold = true;
            this.tabMainAdd.ShowCloseWindowButton = false;
            this.tabMainAdd.ShowToolTip = true;
            this.tabMainAdd.Size = new System.Drawing.Size(443, 355);
            this.tabMainAdd.TabIndex = 1;
            this.tabMainAdd.TabPageBorderColor = System.Drawing.SystemColors.ControlDark;
            this.tabMainAdd.TabPageCloseButtonMouseEnterColor = System.Drawing.Color.Red;
            this.tabMainAdd.TabPageCloseButtonRectangleBorderColor = System.Drawing.SystemColors.Highlight;
            this.tabMainAdd.TabPageCloseButtonRectangleFillColor = System.Drawing.SystemColors.Highlight;
            this.tabMainAdd.TabPageCurrentSelectionIndicatorEndColor = System.Drawing.Color.White;
            this.tabMainAdd.TabPageCurrentSelectionIndicatorStartColor = System.Drawing.Color.Orange;
            this.tabMainAdd.TabPageHeaderDrawingGradientOrientation = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.tabMainAdd.TabPageHeaderForeColor = System.Drawing.SystemColors.WindowText;
            this.tabMainAdd.TabPageHeaderHeight = 24;
            this.tabMainAdd.TabPageHeaderShowsSideBar = false;
            this.tabMainAdd.TabPageHeaderTextFontOverridesControlDefault = false;
            this.tabMainAdd.TabPageHeaderWidth = 200;
            this.tabMainAdd.TabPages.AddRange(new Binarymission.WinFormControls.TabControls.BinaryPowerTabPage[] {
            this.tabCard,
            this.tabDisease,
            this.tabDoctor,
            this.tabService});
            this.tabMainAdd.TabPagesCloseButtonColor = System.Drawing.SystemColors.WindowText;
            this.tabMainAdd.TabPagesHaveGapBetweenThem = false;
            this.tabMainAdd.TabPageSideBarFillColor = System.Drawing.Color.Empty;
            this.tabMainAdd.TabPagesOverFlowMenuDropDownColor = System.Drawing.SystemColors.WindowText;
            this.tabMainAdd.TabPagesOverFlowMenuGlyphActiveRectangleBorderColor = System.Drawing.SystemColors.Highlight;
            this.tabMainAdd.TabPagesOverFlowMenuGlyphActiveRectangleFillColor = System.Drawing.SystemColors.Highlight;
            this.tabMainAdd.TabPagesRenderingLocation = Binarymission.WinFormControls.TabControls.TabPagesRenderingLocation.Top;
            this.tabMainAdd.TabRenderingStyle = Binarymission.WinFormControls.TabControls.Styles.VisualStudio2008;
            this.tabMainAdd.ToolTipConfiguration.AutomaticDelay = 500;
            this.tabMainAdd.ToolTipConfiguration.AutoPopDelay = 5000;
            this.tabMainAdd.ToolTipConfiguration.BackColor = System.Drawing.SystemColors.Info;
            this.tabMainAdd.ToolTipConfiguration.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tabMainAdd.ToolTipConfiguration.InitialDelay = 500;
            this.tabMainAdd.ToolTipConfiguration.IsBalloon = false;
            this.tabMainAdd.ToolTipConfiguration.ReshowDelay = 100;
            this.tabMainAdd.ToolTipConfiguration.StripAmpersands = false;
            this.tabMainAdd.ToolTipConfiguration.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None;
            this.tabMainAdd.ToolTipConfiguration.ToolTipTitle = "";
            this.tabMainAdd.ToolTipConfiguration.UseAnimation = false;
            this.tabMainAdd.ToolTipConfiguration.UseFading = false;
            this.tabMainAdd.UseTabCurrentSelectionIndicatorColor = true;
            // 
            // tabCard
            // 
            this.tabCard.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabCard.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.tabCard.Controls.Add(this.btnCardAdd);
            this.tabCard.Controls.Add(this.tbAddressAdd);
            this.tabCard.Controls.Add(this.lblAddressAdd);
            this.tabCard.Controls.Add(this.dtBirthDayAdd);
            this.tabCard.Controls.Add(this.lblBirthDayAdd);
            this.tabCard.Controls.Add(this.tbInitialsAdd);
            this.tabCard.Controls.Add(this.tbFirstNameAdd);
            this.tabCard.Controls.Add(this.lblInitialsAdd);
            this.tabCard.Controls.Add(this.lblFirstNameAdd);
            this.tabCard.Controls.Add(this.tbLastNameAdd);
            this.tabCard.Controls.Add(this.lblLastNameAdd);
            this.tabCard.CustomEndColor = System.Drawing.Color.Empty;
            this.tabCard.CustomStartColor = System.Drawing.Color.Empty;
            this.tabCard.HeaderTextFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabCard.IsBackgroundTransparant = false;
            this.tabCard.Name = "tabCard";
            this.tabCard.Size = new System.Drawing.Size(441, 329);
            this.tabCard.TabIndex = 0;
            this.tabCard.TabPageContentIsDirty = false;
            this.tabCard.TabPageHeaderForeColor = System.Drawing.Color.Empty;
            this.tabCard.TabPageSideBarFillColor = System.Drawing.Color.Empty;
            this.tabCard.Text = "Карта";
            this.tabCard.ToolTipText = "";
            // 
            // btnCardAdd
            // 
            this.btnCardAdd.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnCardAdd.Location = new System.Drawing.Point(143, 294);
            this.btnCardAdd.Name = "btnCardAdd";
            this.btnCardAdd.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnCardAdd.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnCardAdd.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnCardAdd.OverrideFocus.Border.ColorAngle = 0F;
            this.btnCardAdd.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnCardAdd.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnCardAdd.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCardAdd.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCardAdd.OverrideFocus.Border.Width = 1;
            this.btnCardAdd.Size = new System.Drawing.Size(291, 25);
            this.btnCardAdd.TabIndex = 21;
            this.btnCardAdd.Values.Text = "Добавить";
            this.btnCardAdd.Click += new System.EventHandler(this.btnCardAdd_Click);
            // 
            // tbAddressAdd
            // 
            this.tbAddressAdd.Location = new System.Drawing.Point(142, 111);
            this.tbAddressAdd.Multiline = true;
            this.tbAddressAdd.Name = "tbAddressAdd";
            this.tbAddressAdd.Size = new System.Drawing.Size(292, 177);
            this.tbAddressAdd.TabIndex = 20;
            // 
            // lblAddressAdd
            // 
            this.lblAddressAdd.Location = new System.Drawing.Point(4, 111);
            this.lblAddressAdd.Name = "lblAddressAdd";
            this.lblAddressAdd.Size = new System.Drawing.Size(47, 20);
            this.lblAddressAdd.TabIndex = 19;
            this.lblAddressAdd.Values.Text = "Адрес:";
            // 
            // dtBirthDayAdd
            // 
            this.dtBirthDayAdd.Location = new System.Drawing.Point(142, 84);
            this.dtBirthDayAdd.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtBirthDayAdd.Name = "dtBirthDayAdd";
            this.dtBirthDayAdd.Size = new System.Drawing.Size(135, 21);
            this.dtBirthDayAdd.TabIndex = 18;
            this.dtBirthDayAdd.ValueNullable = new System.DateTime(2011, 3, 11, 18, 1, 47, 0);
            // 
            // lblBirthDayAdd
            // 
            this.lblBirthDayAdd.Location = new System.Drawing.Point(4, 84);
            this.lblBirthDayAdd.Name = "lblBirthDayAdd";
            this.lblBirthDayAdd.Size = new System.Drawing.Size(100, 20);
            this.lblBirthDayAdd.TabIndex = 17;
            this.lblBirthDayAdd.Values.Text = "Дата рождения:";
            // 
            // tbInitialsAdd
            // 
            this.tbInitialsAdd.Location = new System.Drawing.Point(142, 56);
            this.tbInitialsAdd.Name = "tbInitialsAdd";
            this.tbInitialsAdd.Size = new System.Drawing.Size(292, 20);
            this.tbInitialsAdd.TabIndex = 16;
            this.tbInitialsAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // tbFirstNameAdd
            // 
            this.tbFirstNameAdd.Location = new System.Drawing.Point(142, 30);
            this.tbFirstNameAdd.Name = "tbFirstNameAdd";
            this.tbFirstNameAdd.Size = new System.Drawing.Size(292, 20);
            this.tbFirstNameAdd.TabIndex = 15;
            this.tbFirstNameAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // lblInitialsAdd
            // 
            this.lblInitialsAdd.Location = new System.Drawing.Point(4, 57);
            this.lblInitialsAdd.Name = "lblInitialsAdd";
            this.lblInitialsAdd.Size = new System.Drawing.Size(66, 20);
            this.lblInitialsAdd.TabIndex = 14;
            this.lblInitialsAdd.Values.Text = "Отчество:";
            // 
            // lblFirstNameAdd
            // 
            this.lblFirstNameAdd.Location = new System.Drawing.Point(4, 30);
            this.lblFirstNameAdd.Name = "lblFirstNameAdd";
            this.lblFirstNameAdd.Size = new System.Drawing.Size(37, 20);
            this.lblFirstNameAdd.TabIndex = 13;
            this.lblFirstNameAdd.Values.Text = "Имя:";
            // 
            // tbLastNameAdd
            // 
            this.tbLastNameAdd.Location = new System.Drawing.Point(142, 5);
            this.tbLastNameAdd.Name = "tbLastNameAdd";
            this.tbLastNameAdd.Size = new System.Drawing.Size(292, 20);
            this.tbLastNameAdd.TabIndex = 12;
            this.tbLastNameAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // lblLastNameAdd
            // 
            this.lblLastNameAdd.Location = new System.Drawing.Point(3, 3);
            this.lblLastNameAdd.Name = "lblLastNameAdd";
            this.lblLastNameAdd.Size = new System.Drawing.Size(64, 20);
            this.lblLastNameAdd.TabIndex = 11;
            this.lblLastNameAdd.Values.Text = "Фамилия:";
            // 
            // tabDisease
            // 
            this.tabDisease.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabDisease.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.tabDisease.Controls.Add(this.btnDiseaseAdd);
            this.tabDisease.Controls.Add(this.tbDiseaseInfoAdd);
            this.tabDisease.Controls.Add(this.tbDiseaseCathegoryAdd);
            this.tabDisease.Controls.Add(this.tbDiseaseNameAdd);
            this.tabDisease.Controls.Add(this.lblDiseaseInfoAdd);
            this.tabDisease.Controls.Add(this.lblDiseaseCathegoryAdd);
            this.tabDisease.Controls.Add(this.lblDiseaseNameAdd);
            this.tabDisease.CustomEndColor = System.Drawing.Color.Empty;
            this.tabDisease.CustomStartColor = System.Drawing.Color.Empty;
            this.tabDisease.HeaderTextFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabDisease.IsBackgroundTransparant = false;
            this.tabDisease.Name = "tabDisease";
            this.tabDisease.Size = new System.Drawing.Size(441, 329);
            this.tabDisease.TabIndex = 1;
            this.tabDisease.TabPageContentIsDirty = false;
            this.tabDisease.TabPageHeaderForeColor = System.Drawing.Color.Empty;
            this.tabDisease.TabPageSideBarFillColor = System.Drawing.Color.Empty;
            this.tabDisease.Text = "Болезнь";
            this.tabDisease.ToolTipText = "";
            // 
            // btnDiseaseAdd
            // 
            this.btnDiseaseAdd.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnDiseaseAdd.Location = new System.Drawing.Point(142, 294);
            this.btnDiseaseAdd.Name = "btnDiseaseAdd";
            this.btnDiseaseAdd.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnDiseaseAdd.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnDiseaseAdd.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnDiseaseAdd.OverrideFocus.Border.ColorAngle = 0F;
            this.btnDiseaseAdd.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnDiseaseAdd.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnDiseaseAdd.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnDiseaseAdd.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnDiseaseAdd.OverrideFocus.Border.Width = 1;
            this.btnDiseaseAdd.Size = new System.Drawing.Size(290, 25);
            this.btnDiseaseAdd.TabIndex = 19;
            this.btnDiseaseAdd.Values.Text = "Добавить";
            this.btnDiseaseAdd.Click += new System.EventHandler(this.btnDiseaseAdd_Click);
            // 
            // tbDiseaseInfoAdd
            // 
            this.tbDiseaseInfoAdd.Location = new System.Drawing.Point(142, 56);
            this.tbDiseaseInfoAdd.Multiline = true;
            this.tbDiseaseInfoAdd.Name = "tbDiseaseInfoAdd";
            this.tbDiseaseInfoAdd.Size = new System.Drawing.Size(291, 232);
            this.tbDiseaseInfoAdd.TabIndex = 18;
            // 
            // tbDiseaseCathegoryAdd
            // 
            this.tbDiseaseCathegoryAdd.Location = new System.Drawing.Point(142, 30);
            this.tbDiseaseCathegoryAdd.Name = "tbDiseaseCathegoryAdd";
            this.tbDiseaseCathegoryAdd.Size = new System.Drawing.Size(291, 20);
            this.tbDiseaseCathegoryAdd.TabIndex = 17;
            this.tbDiseaseCathegoryAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // tbDiseaseNameAdd
            // 
            this.tbDiseaseNameAdd.Location = new System.Drawing.Point(142, 5);
            this.tbDiseaseNameAdd.Name = "tbDiseaseNameAdd";
            this.tbDiseaseNameAdd.Size = new System.Drawing.Size(291, 20);
            this.tbDiseaseNameAdd.TabIndex = 16;
            this.tbDiseaseNameAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // lblDiseaseInfoAdd
            // 
            this.lblDiseaseInfoAdd.Location = new System.Drawing.Point(3, 56);
            this.lblDiseaseInfoAdd.Name = "lblDiseaseInfoAdd";
            this.lblDiseaseInfoAdd.Size = new System.Drawing.Size(88, 20);
            this.lblDiseaseInfoAdd.TabIndex = 15;
            this.lblDiseaseInfoAdd.Values.Text = "Информация:";
            // 
            // lblDiseaseCathegoryAdd
            // 
            this.lblDiseaseCathegoryAdd.Location = new System.Drawing.Point(3, 29);
            this.lblDiseaseCathegoryAdd.Name = "lblDiseaseCathegoryAdd";
            this.lblDiseaseCathegoryAdd.Size = new System.Drawing.Size(71, 20);
            this.lblDiseaseCathegoryAdd.TabIndex = 14;
            this.lblDiseaseCathegoryAdd.Values.Text = "Категория:";
            // 
            // lblDiseaseNameAdd
            // 
            this.lblDiseaseNameAdd.Location = new System.Drawing.Point(3, 3);
            this.lblDiseaseNameAdd.Name = "lblDiseaseNameAdd";
            this.lblDiseaseNameAdd.Size = new System.Drawing.Size(67, 20);
            this.lblDiseaseNameAdd.TabIndex = 13;
            this.lblDiseaseNameAdd.Values.Text = "Название:";
            // 
            // tabDoctor
            // 
            this.tabDoctor.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabDoctor.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.tabDoctor.Controls.Add(this.tbDoctorInfoAdd);
            this.tabDoctor.Controls.Add(this.tbDoctorPostAdd);
            this.tabDoctor.Controls.Add(this.tbDoctorNameAdd);
            this.tabDoctor.Controls.Add(this.lblDoctorInfoAdd);
            this.tabDoctor.Controls.Add(this.lblDoctorPostAdd);
            this.tabDoctor.Controls.Add(this.lblDoctorNameAdd);
            this.tabDoctor.Controls.Add(this.btnDoctorAdd);
            this.tabDoctor.CustomEndColor = System.Drawing.Color.Empty;
            this.tabDoctor.CustomStartColor = System.Drawing.Color.Empty;
            this.tabDoctor.HeaderTextFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabDoctor.IsBackgroundTransparant = false;
            this.tabDoctor.Name = "tabDoctor";
            this.tabDoctor.Size = new System.Drawing.Size(441, 329);
            this.tabDoctor.TabIndex = 2;
            this.tabDoctor.TabPageContentIsDirty = false;
            this.tabDoctor.TabPageHeaderForeColor = System.Drawing.Color.Empty;
            this.tabDoctor.TabPageSideBarFillColor = System.Drawing.Color.Empty;
            this.tabDoctor.Text = "Доктор";
            this.tabDoctor.ToolTipText = "";
            // 
            // tbDoctorInfoAdd
            // 
            this.tbDoctorInfoAdd.Location = new System.Drawing.Point(142, 56);
            this.tbDoctorInfoAdd.Multiline = true;
            this.tbDoctorInfoAdd.Name = "tbDoctorInfoAdd";
            this.tbDoctorInfoAdd.Size = new System.Drawing.Size(292, 232);
            this.tbDoctorInfoAdd.TabIndex = 28;
            // 
            // tbDoctorPostAdd
            // 
            this.tbDoctorPostAdd.Location = new System.Drawing.Point(142, 30);
            this.tbDoctorPostAdd.Name = "tbDoctorPostAdd";
            this.tbDoctorPostAdd.Size = new System.Drawing.Size(292, 20);
            this.tbDoctorPostAdd.TabIndex = 27;
            this.tbDoctorPostAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // tbDoctorNameAdd
            // 
            this.tbDoctorNameAdd.Location = new System.Drawing.Point(142, 5);
            this.tbDoctorNameAdd.Name = "tbDoctorNameAdd";
            this.tbDoctorNameAdd.Size = new System.Drawing.Size(292, 20);
            this.tbDoctorNameAdd.TabIndex = 26;
            this.tbDoctorNameAdd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // lblDoctorInfoAdd
            // 
            this.lblDoctorInfoAdd.Location = new System.Drawing.Point(4, 56);
            this.lblDoctorInfoAdd.Name = "lblDoctorInfoAdd";
            this.lblDoctorInfoAdd.Size = new System.Drawing.Size(88, 20);
            this.lblDoctorInfoAdd.TabIndex = 25;
            this.lblDoctorInfoAdd.Values.Text = "Информация:";
            // 
            // lblDoctorPostAdd
            // 
            this.lblDoctorPostAdd.Location = new System.Drawing.Point(4, 29);
            this.lblDoctorPostAdd.Name = "lblDoctorPostAdd";
            this.lblDoctorPostAdd.Size = new System.Drawing.Size(76, 20);
            this.lblDoctorPostAdd.TabIndex = 24;
            this.lblDoctorPostAdd.Values.Text = "Должность:";
            // 
            // lblDoctorNameAdd
            // 
            this.lblDoctorNameAdd.Location = new System.Drawing.Point(4, 4);
            this.lblDoctorNameAdd.Name = "lblDoctorNameAdd";
            this.lblDoctorNameAdd.Size = new System.Drawing.Size(41, 20);
            this.lblDoctorNameAdd.TabIndex = 23;
            this.lblDoctorNameAdd.Values.Text = "ФИО:";
            // 
            // btnDoctorAdd
            // 
            this.btnDoctorAdd.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnDoctorAdd.Location = new System.Drawing.Point(143, 294);
            this.btnDoctorAdd.Name = "btnDoctorAdd";
            this.btnDoctorAdd.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnDoctorAdd.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnDoctorAdd.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnDoctorAdd.OverrideFocus.Border.ColorAngle = 0F;
            this.btnDoctorAdd.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnDoctorAdd.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnDoctorAdd.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnDoctorAdd.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnDoctorAdd.OverrideFocus.Border.Width = 1;
            this.btnDoctorAdd.Size = new System.Drawing.Size(291, 25);
            this.btnDoctorAdd.TabIndex = 22;
            this.btnDoctorAdd.Values.Text = "Добавить";
            this.btnDoctorAdd.Click += new System.EventHandler(this.btnDoctorAdd_Click);
            // 
            // tabService
            // 
            this.tabService.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabService.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.tabService.Controls.Add(this.btnSeviceAdd);
            this.tabService.Controls.Add(this.tbServiceInfoAdd);
            this.tabService.Controls.Add(this.tbServicePriceAdd);
            this.tabService.Controls.Add(this.tbServiceNameAdd);
            this.tabService.Controls.Add(this.lblServiceInfoAdd);
            this.tabService.Controls.Add(this.lblServicePriceAdd);
            this.tabService.Controls.Add(this.lblServiceNameAdd);
            this.tabService.CustomEndColor = System.Drawing.Color.Empty;
            this.tabService.CustomStartColor = System.Drawing.Color.Empty;
            this.tabService.HeaderTextFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabService.IsBackgroundTransparant = false;
            this.tabService.Name = "tabService";
            this.tabService.Size = new System.Drawing.Size(441, 329);
            this.tabService.TabIndex = 3;
            this.tabService.TabPageContentIsDirty = false;
            this.tabService.TabPageHeaderForeColor = System.Drawing.Color.Empty;
            this.tabService.TabPageSideBarFillColor = System.Drawing.Color.Empty;
            this.tabService.Text = "Услуга";
            this.tabService.ToolTipText = "";
            // 
            // btnSeviceAdd
            // 
            this.btnSeviceAdd.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnSeviceAdd.Location = new System.Drawing.Point(142, 294);
            this.btnSeviceAdd.Name = "btnSeviceAdd";
            this.btnSeviceAdd.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnSeviceAdd.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnSeviceAdd.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnSeviceAdd.OverrideFocus.Border.ColorAngle = 0F;
            this.btnSeviceAdd.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnSeviceAdd.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnSeviceAdd.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSeviceAdd.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSeviceAdd.OverrideFocus.Border.Width = 1;
            this.btnSeviceAdd.Size = new System.Drawing.Size(291, 25);
            this.btnSeviceAdd.TabIndex = 22;
            this.btnSeviceAdd.Values.Text = "Добавить";
            this.btnSeviceAdd.Click += new System.EventHandler(this.btnSeviceAdd_Click);
            // 
            // tbServiceInfoAdd
            // 
            this.tbServiceInfoAdd.Location = new System.Drawing.Point(142, 56);
            this.tbServiceInfoAdd.Multiline = true;
            this.tbServiceInfoAdd.Name = "tbServiceInfoAdd";
            this.tbServiceInfoAdd.Size = new System.Drawing.Size(291, 232);
            this.tbServiceInfoAdd.TabIndex = 21;
            // 
            // tbServicePriceAdd
            // 
            this.tbServicePriceAdd.Location = new System.Drawing.Point(142, 30);
            this.tbServicePriceAdd.Name = "tbServicePriceAdd";
            this.tbServicePriceAdd.Size = new System.Drawing.Size(291, 20);
            this.tbServicePriceAdd.TabIndex = 20;
            // 
            // tbServiceNameAdd
            // 
            this.tbServiceNameAdd.Location = new System.Drawing.Point(142, 5);
            this.tbServiceNameAdd.Name = "tbServiceNameAdd";
            this.tbServiceNameAdd.Size = new System.Drawing.Size(291, 20);
            this.tbServiceNameAdd.TabIndex = 19;
            // 
            // lblServiceInfoAdd
            // 
            this.lblServiceInfoAdd.Location = new System.Drawing.Point(3, 56);
            this.lblServiceInfoAdd.Name = "lblServiceInfoAdd";
            this.lblServiceInfoAdd.Size = new System.Drawing.Size(70, 20);
            this.lblServiceInfoAdd.TabIndex = 18;
            this.lblServiceInfoAdd.Values.Text = "Описание:";
            // 
            // lblServicePriceAdd
            // 
            this.lblServicePriceAdd.Location = new System.Drawing.Point(3, 30);
            this.lblServicePriceAdd.Name = "lblServicePriceAdd";
            this.lblServicePriceAdd.Size = new System.Drawing.Size(72, 20);
            this.lblServicePriceAdd.TabIndex = 17;
            this.lblServicePriceAdd.Values.Text = "Цена (грн.)";
            // 
            // lblServiceNameAdd
            // 
            this.lblServiceNameAdd.Location = new System.Drawing.Point(3, 3);
            this.lblServiceNameAdd.Name = "lblServiceNameAdd";
            this.lblServiceNameAdd.Size = new System.Drawing.Size(67, 20);
            this.lblServiceNameAdd.TabIndex = 16;
            this.lblServiceNameAdd.Values.Text = "Название:";
            // 
            // btnCancel
            // 
            this.btnCancel.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnCancel.Location = new System.Drawing.Point(319, 396);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnCancel.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnCancel.OverrideFocus.Border.ColorAngle = 0F;
            this.btnCancel.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnCancel.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnCancel.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCancel.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCancel.OverrideFocus.Border.Width = 1;
            this.btnCancel.Size = new System.Drawing.Size(135, 25);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Values.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // validationWarningToolTip
            // 
            this.validationWarningToolTip.IsBalloon = true;
            // 
            // btnDone
            // 
            this.btnDone.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.btnDone.Location = new System.Drawing.Point(158, 396);
            this.btnDone.Name = "btnDone";
            this.btnDone.OverrideFocus.Border.Color1 = System.Drawing.SystemColors.ActiveCaption;
            this.btnDone.OverrideFocus.Border.Color2 = System.Drawing.SystemColors.ButtonFace;
            this.btnDone.OverrideFocus.Border.ColorAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Control;
            this.btnDone.OverrideFocus.Border.ColorAngle = 0F;
            this.btnDone.OverrideFocus.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.SolidBottomLine;
            this.btnDone.OverrideFocus.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btnDone.OverrideFocus.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)
                        | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnDone.OverrideFocus.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnDone.OverrideFocus.Border.Width = 1;
            this.btnDone.Size = new System.Drawing.Size(135, 25);
            this.btnDone.TabIndex = 16;
            this.btnDone.Values.Text = "Готово";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // frmAddEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(476, 428);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.grboxAddNew);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAddEdit";
            this.Text = "frmAddNew";
            this.Load += new System.EventHandler(this.frmAddEdit_Load);
            this.grboxAddNew.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grboxAddNew)).EndInit();
            this.grboxAddNew.ResumeLayout(false);
            this.tabMainAdd.ResumeLayout(false);
            this.tabCard.ResumeLayout(false);
            this.tabCard.PerformLayout();
            this.tabDisease.ResumeLayout(false);
            this.tabDisease.PerformLayout();
            this.tabDoctor.ResumeLayout(false);
            this.tabDoctor.PerformLayout();
            this.tabService.ResumeLayout(false);
            this.tabService.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonManager kryptonManager1;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox grboxAddNew;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCancel;
        private System.Windows.Forms.ToolTip validationWarningToolTip;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDone;
        private BinaryPowerTabStrip tabMainAdd;
        private BinaryPowerTabPage tabCard;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCardAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbAddressAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblAddressAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dtBirthDayAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblBirthDayAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbInitialsAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbFirstNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblInitialsAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblFirstNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbLastNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblLastNameAdd;
        private BinaryPowerTabPage tabDisease;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDiseaseAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiseaseInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiseaseCathegoryAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiseaseNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiseaseInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiseaseCathegoryAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiseaseNameAdd;
        private BinaryPowerTabPage tabDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDoctorInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDoctorPostAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDoctorNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDoctorInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDoctorPostAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDoctorNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDoctorAdd;
        private BinaryPowerTabPage tabService;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSeviceAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbServiceInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbServicePriceAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbServiceNameAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServiceInfoAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServicePriceAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServiceNameAdd;
    }
}