﻿namespace Aurora.Core.ViewForms
{
    partial class frmVisit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kgbVisitStep1 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnMoreAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cmbVisitLastName = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.dtVisitDate = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.lblVisitDiagnosisInfo = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblVisitCard = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblVisitDoctor = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cmbVisitDoctor = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kgbVisitStep2 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnDiagnosisAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblDiagnosisInfo = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbDiagnosisInfo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblVisitDiagnosis = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cmbVisitDisease = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kgbDignosisList = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnRemoveDiagnosis = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dgDiagnosis = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Info = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kgbVisitStep3 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.lbxServices = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.lblvisitServices = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kgbVisitSummary = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnCancel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnPrint = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnDone = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblPriceSum = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblNumServices = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblPrice = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblServices = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep1)).BeginInit();
            this.kgbVisitStep1.Panel.SuspendLayout();
            this.kgbVisitStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDoctor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep2)).BeginInit();
            this.kgbVisitStep2.Panel.SuspendLayout();
            this.kgbVisitStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDisease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kgbDignosisList)).BeginInit();
            this.kgbDignosisList.Panel.SuspendLayout();
            this.kgbDignosisList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagnosis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep3)).BeginInit();
            this.kgbVisitStep3.Panel.SuspendLayout();
            this.kgbVisitStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitSummary)).BeginInit();
            this.kgbVisitSummary.Panel.SuspendLayout();
            this.kgbVisitSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // kgbVisitStep1
            // 
            this.kgbVisitStep1.Location = new System.Drawing.Point(12, 13);
            this.kgbVisitStep1.Name = "kgbVisitStep1";
            // 
            // kgbVisitStep1.Panel
            // 
            this.kgbVisitStep1.Panel.Controls.Add(this.btnMoreAdd);
            this.kgbVisitStep1.Panel.Controls.Add(this.cmbVisitLastName);
            this.kgbVisitStep1.Panel.Controls.Add(this.dtVisitDate);
            this.kgbVisitStep1.Panel.Controls.Add(this.lblVisitDiagnosisInfo);
            this.kgbVisitStep1.Panel.Controls.Add(this.lblVisitCard);
            this.kgbVisitStep1.Panel.Controls.Add(this.lblVisitDoctor);
            this.kgbVisitStep1.Panel.Controls.Add(this.cmbVisitDoctor);
            this.kgbVisitStep1.Size = new System.Drawing.Size(199, 260);
            this.kgbVisitStep1.TabIndex = 12;
            this.kgbVisitStep1.Values.Heading = "Шаг 1";
            // 
            // btnMoreAdd
            // 
            this.btnMoreAdd.Location = new System.Drawing.Point(3, 208);
            this.btnMoreAdd.Name = "btnMoreAdd";
            this.btnMoreAdd.Size = new System.Drawing.Size(188, 25);
            this.btnMoreAdd.TabIndex = 18;
            this.btnMoreAdd.Values.Text = "Еще";
            this.btnMoreAdd.Click += new System.EventHandler(this.btnMoreAdd_Click);
            // 
            // cmbVisitLastName
            // 
            this.cmbVisitLastName.DropDownWidth = 188;
            this.cmbVisitLastName.Location = new System.Drawing.Point(3, 31);
            this.cmbVisitLastName.Name = "cmbVisitLastName";
            this.cmbVisitLastName.Size = new System.Drawing.Size(188, 21);
            this.cmbVisitLastName.TabIndex = 11;
            this.cmbVisitLastName.Text = "Выберите пациента";
            this.cmbVisitLastName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbVisitLastName_KeyUp);
            // 
            // dtVisitDate
            // 
            this.dtVisitDate.Location = new System.Drawing.Point(3, 137);
            this.dtVisitDate.Name = "dtVisitDate";
            this.dtVisitDate.Size = new System.Drawing.Size(188, 21);
            this.dtVisitDate.TabIndex = 13;
            // 
            // lblVisitDiagnosisInfo
            // 
            this.lblVisitDiagnosisInfo.Location = new System.Drawing.Point(4, 111);
            this.lblVisitDiagnosisInfo.Name = "lblVisitDiagnosisInfo";
            this.lblVisitDiagnosisInfo.Size = new System.Drawing.Size(37, 20);
            this.lblVisitDiagnosisInfo.TabIndex = 17;
            this.lblVisitDiagnosisInfo.Values.Text = "Дата";
            // 
            // lblVisitCard
            // 
            this.lblVisitCard.Location = new System.Drawing.Point(3, 5);
            this.lblVisitCard.Name = "lblVisitCard";
            this.lblVisitCard.Size = new System.Drawing.Size(59, 20);
            this.lblVisitCard.TabIndex = 15;
            this.lblVisitCard.Values.Text = "Пациент";
            // 
            // lblVisitDoctor
            // 
            this.lblVisitDoctor.Location = new System.Drawing.Point(3, 58);
            this.lblVisitDoctor.Name = "lblVisitDoctor";
            this.lblVisitDoctor.Size = new System.Drawing.Size(38, 20);
            this.lblVisitDoctor.TabIndex = 16;
            this.lblVisitDoctor.Values.Text = "Врач";
            // 
            // cmbVisitDoctor
            // 
            this.cmbVisitDoctor.DropDownWidth = 188;
            this.cmbVisitDoctor.Location = new System.Drawing.Point(3, 84);
            this.cmbVisitDoctor.Name = "cmbVisitDoctor";
            this.cmbVisitDoctor.Size = new System.Drawing.Size(188, 21);
            this.cmbVisitDoctor.TabIndex = 12;
            this.cmbVisitDoctor.Text = "Лечащий врач";
            this.cmbVisitDoctor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbVisitDoctor_KeyUp);
            // 
            // kgbVisitStep2
            // 
            this.kgbVisitStep2.Location = new System.Drawing.Point(217, 13);
            this.kgbVisitStep2.Name = "kgbVisitStep2";
            // 
            // kgbVisitStep2.Panel
            // 
            this.kgbVisitStep2.Panel.Controls.Add(this.btnDiagnosisAdd);
            this.kgbVisitStep2.Panel.Controls.Add(this.lblDiagnosisInfo);
            this.kgbVisitStep2.Panel.Controls.Add(this.tbDiagnosisInfo);
            this.kgbVisitStep2.Panel.Controls.Add(this.lblVisitDiagnosis);
            this.kgbVisitStep2.Panel.Controls.Add(this.cmbVisitDisease);
            this.kgbVisitStep2.Size = new System.Drawing.Size(247, 260);
            this.kgbVisitStep2.TabIndex = 13;
            this.kgbVisitStep2.Values.Heading = "Шаг 2";
            // 
            // btnDiagnosisAdd
            // 
            this.btnDiagnosisAdd.Location = new System.Drawing.Point(3, 208);
            this.btnDiagnosisAdd.Name = "btnDiagnosisAdd";
            this.btnDiagnosisAdd.Size = new System.Drawing.Size(237, 25);
            this.btnDiagnosisAdd.TabIndex = 14;
            this.btnDiagnosisAdd.Values.Text = "Добавить";
            this.btnDiagnosisAdd.Click += new System.EventHandler(this.btnDiagnosisAdd_Click);
            // 
            // lblDiagnosisInfo
            // 
            this.lblDiagnosisInfo.Location = new System.Drawing.Point(3, 59);
            this.lblDiagnosisInfo.Name = "lblDiagnosisInfo";
            this.lblDiagnosisInfo.Size = new System.Drawing.Size(60, 20);
            this.lblDiagnosisInfo.TabIndex = 13;
            this.lblDiagnosisInfo.Values.Text = "Анамнез";
            // 
            // tbDiagnosisInfo
            // 
            this.tbDiagnosisInfo.Location = new System.Drawing.Point(3, 85);
            this.tbDiagnosisInfo.Multiline = true;
            this.tbDiagnosisInfo.Name = "tbDiagnosisInfo";
            this.tbDiagnosisInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDiagnosisInfo.Size = new System.Drawing.Size(237, 117);
            this.tbDiagnosisInfo.TabIndex = 12;
            this.tbDiagnosisInfo.WordWrap = false;
            // 
            // lblVisitDiagnosis
            // 
            this.lblVisitDiagnosis.Location = new System.Drawing.Point(3, 5);
            this.lblVisitDiagnosis.Name = "lblVisitDiagnosis";
            this.lblVisitDiagnosis.Size = new System.Drawing.Size(57, 20);
            this.lblVisitDiagnosis.TabIndex = 11;
            this.lblVisitDiagnosis.Values.Text = "Диагноз";
            // 
            // cmbVisitDisease
            // 
            this.cmbVisitDisease.DropDownWidth = 188;
            this.cmbVisitDisease.Location = new System.Drawing.Point(3, 31);
            this.cmbVisitDisease.Name = "cmbVisitDisease";
            this.cmbVisitDisease.Size = new System.Drawing.Size(237, 21);
            this.cmbVisitDisease.TabIndex = 10;
            this.cmbVisitDisease.Text = "Заболевание";
            this.cmbVisitDisease.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbVisitDisease_KeyUp);
            // 
            // kgbDignosisList
            // 
            this.kgbDignosisList.Location = new System.Drawing.Point(12, 280);
            this.kgbDignosisList.Name = "kgbDignosisList";
            // 
            // kgbDignosisList.Panel
            // 
            this.kgbDignosisList.Panel.Controls.Add(this.btnRemoveDiagnosis);
            this.kgbDignosisList.Panel.Controls.Add(this.dgDiagnosis);
            this.kgbDignosisList.Size = new System.Drawing.Size(452, 201);
            this.kgbDignosisList.TabIndex = 14;
            this.kgbDignosisList.Values.Heading = "Диагнозы";
            // 
            // btnRemoveDiagnosis
            // 
            this.btnRemoveDiagnosis.Location = new System.Drawing.Point(410, 4);
            this.btnRemoveDiagnosis.Name = "btnRemoveDiagnosis";
            this.btnRemoveDiagnosis.Size = new System.Drawing.Size(35, 25);
            this.btnRemoveDiagnosis.TabIndex = 1;
            this.btnRemoveDiagnosis.Values.Text = "-";
            this.btnRemoveDiagnosis.Click += new System.EventHandler(this.btnRemoveDiagnosis_Click);
            // 
            // dgDiagnosis
            // 
            this.dgDiagnosis.AllowUserToAddRows = false;
            this.dgDiagnosis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDiagnosis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.DisName,
            this.Info});
            this.dgDiagnosis.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgDiagnosis.Location = new System.Drawing.Point(4, 4);
            this.dgDiagnosis.Name = "dgDiagnosis";
            this.dgDiagnosis.ReadOnly = true;
            this.dgDiagnosis.RowHeadersVisible = false;
            this.dgDiagnosis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiagnosis.Size = new System.Drawing.Size(399, 170);
            this.dgDiagnosis.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Id.HeaderText = "ИН";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Id.Width = 54;
            // 
            // DisName
            // 
            this.DisName.HeaderText = "Диагноз";
            this.DisName.Name = "DisName";
            this.DisName.ReadOnly = true;
            // 
            // Info
            // 
            this.Info.HeaderText = "Анамнез";
            this.Info.Name = "Info";
            this.Info.ReadOnly = true;
            // 
            // kgbVisitStep3
            // 
            this.kgbVisitStep3.Location = new System.Drawing.Point(471, 13);
            this.kgbVisitStep3.Name = "kgbVisitStep3";
            // 
            // kgbVisitStep3.Panel
            // 
            this.kgbVisitStep3.Panel.Controls.Add(this.lbxServices);
            this.kgbVisitStep3.Panel.Controls.Add(this.lblvisitServices);
            this.kgbVisitStep3.Size = new System.Drawing.Size(258, 260);
            this.kgbVisitStep3.TabIndex = 15;
            this.kgbVisitStep3.Values.Heading = "Шаг 3";
            // 
            // lbxServices
            // 
            this.lbxServices.Location = new System.Drawing.Point(4, 31);
            this.lbxServices.Name = "lbxServices";
            this.lbxServices.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxServices.Size = new System.Drawing.Size(247, 202);
            this.lbxServices.Sorted = true;
            this.lbxServices.TabIndex = 14;
            this.lbxServices.SelectedValueChanged += new System.EventHandler(this.lbxServices_SelectedValueChanged);
            // 
            // lblvisitServices
            // 
            this.lblvisitServices.Location = new System.Drawing.Point(4, 5);
            this.lblvisitServices.Name = "lblvisitServices";
            this.lblvisitServices.Size = new System.Drawing.Size(113, 20);
            this.lblvisitServices.TabIndex = 13;
            this.lblvisitServices.Values.Text = "Оказанные услуги";
            // 
            // kgbVisitSummary
            // 
            this.kgbVisitSummary.Location = new System.Drawing.Point(471, 280);
            this.kgbVisitSummary.Name = "kgbVisitSummary";
            // 
            // kgbVisitSummary.Panel
            // 
            this.kgbVisitSummary.Panel.Controls.Add(this.btnCancel);
            this.kgbVisitSummary.Panel.Controls.Add(this.btnPrint);
            this.kgbVisitSummary.Panel.Controls.Add(this.btnDone);
            this.kgbVisitSummary.Panel.Controls.Add(this.lblPriceSum);
            this.kgbVisitSummary.Panel.Controls.Add(this.lblNumServices);
            this.kgbVisitSummary.Panel.Controls.Add(this.lblPrice);
            this.kgbVisitSummary.Panel.Controls.Add(this.lblServices);
            this.kgbVisitSummary.Size = new System.Drawing.Size(258, 201);
            this.kgbVisitSummary.TabIndex = 16;
            this.kgbVisitSummary.Values.Heading = "Итог";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(124, 152);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(127, 25);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Values.Text = "Отмена";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(124, 119);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(127, 25);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Values.Text = "Печать";
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(124, 88);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(127, 25);
            this.btnDone.TabIndex = 4;
            this.btnDone.Values.Text = "Готово";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // lblPriceSum
            // 
            this.lblPriceSum.Location = new System.Drawing.Point(163, 30);
            this.lblPriceSum.Name = "lblPriceSum";
            this.lblPriceSum.Size = new System.Drawing.Size(42, 20);
            this.lblPriceSum.TabIndex = 3;
            this.lblPriceSum.Values.Text = "0 грн.";
            // 
            // lblNumServices
            // 
            this.lblNumServices.Location = new System.Drawing.Point(163, 4);
            this.lblNumServices.Name = "lblNumServices";
            this.lblNumServices.Size = new System.Drawing.Size(17, 20);
            this.lblNumServices.TabIndex = 2;
            this.lblNumServices.Values.Text = "0";
            // 
            // lblPrice
            // 
            this.lblPrice.Location = new System.Drawing.Point(4, 31);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(91, 20);
            this.lblPrice.TabIndex = 1;
            this.lblPrice.Values.Text = "Общая сумма:";
            // 
            // lblServices
            // 
            this.lblServices.Location = new System.Drawing.Point(4, 4);
            this.lblServices.Name = "lblServices";
            this.lblServices.Size = new System.Drawing.Size(93, 20);
            this.lblServices.TabIndex = 0;
            this.lblServices.Values.Text = "Оказано услуг:";
            // 
            // frmVisit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(741, 493);
            this.Controls.Add(this.kgbVisitSummary);
            this.Controls.Add(this.kgbVisitStep3);
            this.Controls.Add(this.kgbDignosisList);
            this.Controls.Add(this.kgbVisitStep2);
            this.Controls.Add(this.kgbVisitStep1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVisit";
            this.Text = "Прием";
            this.Load += new System.EventHandler(this.frmVisit_Load);
            this.kgbVisitStep1.Panel.ResumeLayout(false);
            this.kgbVisitStep1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep1)).EndInit();
            this.kgbVisitStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDoctor)).EndInit();
            this.kgbVisitStep2.Panel.ResumeLayout(false);
            this.kgbVisitStep2.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep2)).EndInit();
            this.kgbVisitStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDisease)).EndInit();
            this.kgbDignosisList.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kgbDignosisList)).EndInit();
            this.kgbDignosisList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDiagnosis)).EndInit();
            this.kgbVisitStep3.Panel.ResumeLayout(false);
            this.kgbVisitStep3.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitStep3)).EndInit();
            this.kgbVisitStep3.ResumeLayout(false);
            this.kgbVisitSummary.Panel.ResumeLayout(false);
            this.kgbVisitSummary.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kgbVisitSummary)).EndInit();
            this.kgbVisitSummary.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kgbVisitStep1;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kgbVisitStep2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDiagnosisAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiagnosisInfo;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiagnosisInfo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitDiagnosis;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbVisitDisease;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnMoreAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitDiagnosisInfo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitCard;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dtVisitDate;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbVisitDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbVisitLastName;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kgbDignosisList;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kgbVisitStep3;
        private ComponentFactory.Krypton.Toolkit.KryptonListBox lbxServices;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblvisitServices;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnRemoveDiagnosis;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgDiagnosis;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kgbVisitSummary;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblPrice;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServices;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCancel;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnPrint;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDone;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblPriceSum;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblNumServices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Info;
    }
}