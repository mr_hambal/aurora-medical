using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Aurora.Domain;
using Aurora.Infrastructure;

namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        private void FilterVisits()
        {
            if (tabMainNavi.SelectedIndex != 1)
            {
                return;
            }

            var oldData = (IEnumerable<Visit>)_dgDataSource;

            dgVisits.DataSource = oldData.Intersection(oldData.Return(t => t.Where(x => x.VisitDoctor.Name.Contains(tbVisitDoctor.Text)), oldData)).
                Intersection(oldData.Return(t => t.Where
                                            (x => x.VisitDiagnosis.Contains
                                                (
                                                    (from diag in x.VisitDiagnosis
                                                     where diag.DiagnosisDisease.Name.Contains(tbVisitDisease.Text)
                                                     select diag).FirstOrDefault()
                                                )
                                            ),
                                            oldData)).
                Intersection(oldData.Return(t => t.Where(x => x.RelatedCard.LastName.Contains(tbVisitCard.Text)), oldData)).
                Intersection(Filters.FilterDate(oldData, "VisitDate", dtVisitDate.Value,
                                                cmbVisitDate.SelectedIndex));
                InitVisitColumns();
            return;
        }

        /// <summary>
        /// Call filtering by Visits on change some parameters.
        /// </summary>
        /// <param name="sender">Control</param>
        /// <param name="e"></param>
        private void VisitsParametersChanged(object sender, EventArgs e)
        {
            try
            {
                BeginInvoke(new InvokeDelegate(FilterVisits));
            }
            catch (InvalidOperationException invalidOperationException)
            {
                Messenger.ShowErrorMessage("������!", "���������� ��� ���. " + invalidOperationException.Message);
            }
            return;
        }

        private void VisitsAsDataSource()
        {
            _dgDataSource = ((AuroraEntities)_repository.GetContext()).Visits.
                Include("VisitDoctor").
                Include("RelatedCard").
                Include("Services").
                Include("VisitDiagnosis.DiagnosisDisease").
                ToList();

            dgVisits.DataSource = _dgDataSource;
            InitVisitColumns();
            return;
        }

        private void InitVisitColumns()
        {
            dgVisits.Columns.Clear();

            dgVisits.CellContentClick += dgVisits_CellContentClick;

            dgVisits.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Id", HeaderText = "��" });
            dgVisits.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "VisitDate", HeaderText = "����" });
            dgVisits.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "RelatedCard", HeaderText = "�������" });
            dgVisits.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "VisitDoctor", HeaderText = "������" });
            dgVisits.Columns.Add(new DataGridViewButtonColumn() { DataPropertyName = "Services", HeaderText = "������", UseColumnTextForButtonValue = true });
            dgVisits.Columns.Add(new DataGridViewButtonColumn() { DataPropertyName = "VisitDiagnosis", HeaderText = "��������", UseColumnTextForButtonValue = true });
            return;
        }

        private void dgVisits_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 4:
                    ShowServices(e.RowIndex);
                    break;
                case 5:
                    ShowDiagnoses(e.RowIndex);
                    break;
                default:
                    return;
            }
        }

        private void ShowServices(int row)
        {
            var visitId = (Int64) dgVisits.Rows[row].Cells[0].Value;

            tabMainNavi.SelectTab(3);

            var dataSource = (IEnumerable<Service>)_dgDataSource;

            dgServices.DataSource = dataSource.Intersection(
                                                            (from service in dataSource 
                                                                from visit in service.Visits 
                                                                where visit.Id == visitId 
                                                             select service).ToList()
                                                        );
            return;
        }

        private void ShowDiagnoses(int row)
        {
            try
            {
                var id = (Int64)dgVisits.Rows[row].Cells[0].Value;
                var visitFrm = new frmVisit(_repository, null);
                visitFrm.ShowForView(id);
            }
            catch (ArgumentOutOfRangeException argumentOutOfRangeException)
            {
                Messenger.ShowErrorMessage("������ � ����������", argumentOutOfRangeException.Message);
            }

            return;
        }
    }
}