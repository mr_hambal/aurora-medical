﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Shinigami
 * Дата: 04.02.2011
 * Время: 11:34
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mgrMainKryptonManager = new ComponentFactory.Krypton.Toolkit.KryptonManager(this.components);
            this.pnlControls = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.btnVisit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnExit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnAdd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cmEditDelete = new ComponentFactory.Krypton.Toolkit.KryptonContextMenu();
            this.cmItems = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems();
            this.mnuEdit = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.mnuDelete = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.mnuVisits = new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem();
            this.tabMainNavi = new System.Windows.Forms.TabControl();
            this.tabCards = new System.Windows.Forms.TabPage();
            this.dgCards = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnlCards = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cmbSearchDateSwitch = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.tbSearchAddress = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblSearchAddress = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.dtSearchBirth = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.lblSearchBirth = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbSearchLastName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbSearchLastNameEquals = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblSearchLastName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbSearchInitials = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbSearchInitialsEquals = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblSearchInitials = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbSearchFirstName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbSearchFirstNameEquals = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblSearchFirstName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabVisits = new System.Windows.Forms.TabPage();
            this.dgVisits = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnlVisits = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cmbVisitDate = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.dtVisitDate = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.lblVisitsDate = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbVisitCard = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbVisitCard = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblVisitCard = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbVisitDisease = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbVisitsDisease = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblVisitDisease = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbVisitDoctor = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbVisitDoctor = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblVisitDoctor = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabDiseases = new System.Windows.Forms.TabPage();
            this.dgDiseases = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnlDiseases = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.tbDiseaseCathegory = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbDiseaseCathegory = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblDiseaseCathegory = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbDiseaseName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbDiseaseName = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblDiseaseName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabServices = new System.Windows.Forms.TabPage();
            this.dgServices = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnlServices = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cmbServicesPrice = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.tbServicesPrice = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.lblServicesPrice = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbServicesName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbServicesName = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblServicesName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tabDoctors = new System.Windows.Forms.TabPage();
            this.dgDoctors = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.pnlDoctors = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.tbDoctorsPost = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbDoctorsPost = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblDoctorPost = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbDoctorsName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.chbDoctorsName = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.lblDoctorsName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pnlControls)).BeginInit();
            this.pnlControls.SuspendLayout();
            this.tabMainNavi.SuspendLayout();
            this.tabCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCards)).BeginInit();
            this.pnlCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSearchDateSwitch)).BeginInit();
            this.tabVisits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisits)).BeginInit();
            this.pnlVisits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDate)).BeginInit();
            this.tabDiseases.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDiseases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDiseases)).BeginInit();
            this.pnlDiseases.SuspendLayout();
            this.tabServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlServices)).BeginInit();
            this.pnlServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServicesPrice)).BeginInit();
            this.tabDoctors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDoctors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDoctors)).BeginInit();
            this.pnlDoctors.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.btnVisit);
            this.pnlControls.Controls.Add(this.btnExit);
            this.pnlControls.Controls.Add(this.btnAdd);
            this.pnlControls.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlControls.Location = new System.Drawing.Point(0, 0);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(100, 362);
            this.pnlControls.TabIndex = 1;
            // 
            // btnVisit
            // 
            this.btnVisit.Location = new System.Drawing.Point(0, 52);
            this.btnVisit.Name = "btnVisit";
            this.btnVisit.Size = new System.Drawing.Size(100, 30);
            this.btnVisit.TabIndex = 2;
            this.btnVisit.Values.Text = "Посещение";
            this.btnVisit.Click += new System.EventHandler(this.btnVisit_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(0, 81);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 25);
            this.btnExit.TabIndex = 1;
            this.btnExit.Values.Text = "Выход";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(0, 26);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 30);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Values.Text = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmEditDelete
            // 
            this.cmEditDelete.Items.AddRange(new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase[] {
            this.cmItems});
            this.cmEditDelete.Opening += new System.ComponentModel.CancelEventHandler(this.cmEditDelete_Opening);
            // 
            // cmItems
            // 
            this.cmItems.Items.AddRange(new ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase[] {
            this.mnuEdit,
            this.mnuDelete,
            this.mnuVisits});
            // 
            // mnuEdit
            // 
            this.mnuEdit.Text = "Редактировать";
            this.mnuEdit.Click += new System.EventHandler(this.kryptonContextEdit_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.CheckOnClick = true;
            this.mnuDelete.Text = "Удалить";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuVisits
            // 
            this.mnuVisits.Text = "Приемы";
            this.mnuVisits.Visible = false;
            this.mnuVisits.Click += new System.EventHandler(this.mnuVisits_Click);
            // 
            // tabMainNavi
            // 
            this.tabMainNavi.Controls.Add(this.tabCards);
            this.tabMainNavi.Controls.Add(this.tabVisits);
            this.tabMainNavi.Controls.Add(this.tabDiseases);
            this.tabMainNavi.Controls.Add(this.tabServices);
            this.tabMainNavi.Controls.Add(this.tabDoctors);
            this.tabMainNavi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMainNavi.Location = new System.Drawing.Point(100, 0);
            this.tabMainNavi.Margin = new System.Windows.Forms.Padding(0);
            this.tabMainNavi.Name = "tabMainNavi";
            this.tabMainNavi.Padding = new System.Drawing.Point(0, 0);
            this.tabMainNavi.SelectedIndex = 0;
            this.tabMainNavi.Size = new System.Drawing.Size(606, 362);
            this.tabMainNavi.TabIndex = 2;
            this.tabMainNavi.SelectedIndexChanged += new System.EventHandler(this.tabMain_tabChange);
            // 
            // tabCards
            // 
            this.tabCards.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.tabCards.Controls.Add(this.dgCards);
            this.tabCards.Controls.Add(this.pnlCards);
            this.tabCards.Location = new System.Drawing.Point(4, 22);
            this.tabCards.Name = "tabCards";
            this.tabCards.Size = new System.Drawing.Size(598, 336);
            this.tabCards.TabIndex = 0;
            this.tabCards.Text = "Карточки";
            // 
            // dgCards
            // 
            this.dgCards.AllowUserToAddRows = false;
            this.dgCards.AllowUserToDeleteRows = false;
            this.dgCards.AllowUserToOrderColumns = true;
            this.dgCards.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgCards.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCards.Location = new System.Drawing.Point(0, 84);
            this.dgCards.MultiSelect = false;
            this.dgCards.Name = "dgCards";
            this.dgCards.ReadOnly = true;
            this.dgCards.RowHeadersVisible = false;
            this.dgCards.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCards.Size = new System.Drawing.Size(598, 252);
            this.dgCards.TabIndex = 3;
            this.dgCards.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_CellMouseClick);
            // 
            // pnlCards
            // 
            this.pnlCards.Controls.Add(this.cmbSearchDateSwitch);
            this.pnlCards.Controls.Add(this.tbSearchAddress);
            this.pnlCards.Controls.Add(this.lblSearchAddress);
            this.pnlCards.Controls.Add(this.dtSearchBirth);
            this.pnlCards.Controls.Add(this.lblSearchBirth);
            this.pnlCards.Controls.Add(this.tbSearchLastName);
            this.pnlCards.Controls.Add(this.chbSearchLastNameEquals);
            this.pnlCards.Controls.Add(this.lblSearchLastName);
            this.pnlCards.Controls.Add(this.tbSearchInitials);
            this.pnlCards.Controls.Add(this.chbSearchInitialsEquals);
            this.pnlCards.Controls.Add(this.lblSearchInitials);
            this.pnlCards.Controls.Add(this.tbSearchFirstName);
            this.pnlCards.Controls.Add(this.chbSearchFirstNameEquals);
            this.pnlCards.Controls.Add(this.lblSearchFirstName);
            this.pnlCards.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCards.Location = new System.Drawing.Point(0, 0);
            this.pnlCards.Name = "pnlCards";
            this.pnlCards.Size = new System.Drawing.Size(598, 84);
            this.pnlCards.TabIndex = 2;
            // 
            // cmbSearchDateSwitch
            // 
            this.cmbSearchDateSwitch.DropDownWidth = 45;
            this.cmbSearchDateSwitch.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.cmbSearchDateSwitch.Location = new System.Drawing.Point(414, 2);
            this.cmbSearchDateSwitch.Name = "cmbSearchDateSwitch";
            this.cmbSearchDateSwitch.Size = new System.Drawing.Size(45, 21);
            this.cmbSearchDateSwitch.TabIndex = 14;
            this.cmbSearchDateSwitch.SelectedIndexChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // tbSearchAddress
            // 
            this.tbSearchAddress.Location = new System.Drawing.Point(465, 32);
            this.tbSearchAddress.Name = "tbSearchAddress";
            this.tbSearchAddress.Size = new System.Drawing.Size(117, 20);
            this.tbSearchAddress.TabIndex = 13;
            this.tbSearchAddress.TextChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // lblSearchAddress
            // 
            this.lblSearchAddress.Location = new System.Drawing.Point(318, 29);
            this.lblSearchAddress.Name = "lblSearchAddress";
            this.lblSearchAddress.Size = new System.Drawing.Size(47, 20);
            this.lblSearchAddress.TabIndex = 12;
            this.lblSearchAddress.Values.Text = "Адрес:";
            // 
            // dtSearchBirth
            // 
            this.dtSearchBirth.CalendarTodayDate = new System.DateTime(2011, 2, 27, 0, 0, 0, 0);
            this.dtSearchBirth.Location = new System.Drawing.Point(465, 3);
            this.dtSearchBirth.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtSearchBirth.Name = "dtSearchBirth";
            this.dtSearchBirth.Size = new System.Drawing.Size(117, 21);
            this.dtSearchBirth.TabIndex = 11;
            this.dtSearchBirth.ValueChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // lblSearchBirth
            // 
            this.lblSearchBirth.Location = new System.Drawing.Point(318, 4);
            this.lblSearchBirth.Name = "lblSearchBirth";
            this.lblSearchBirth.Size = new System.Drawing.Size(100, 20);
            this.lblSearchBirth.TabIndex = 10;
            this.lblSearchBirth.Values.Text = "Дата рождения:";
            // 
            // tbSearchLastName
            // 
            this.tbSearchLastName.Location = new System.Drawing.Point(191, 56);
            this.tbSearchLastName.Name = "tbSearchLastName";
            this.tbSearchLastName.Size = new System.Drawing.Size(121, 20);
            this.tbSearchLastName.TabIndex = 9;
            this.tbSearchLastName.TextChanged += new System.EventHandler(this.CardsParameterChanged);
            this.tbSearchLastName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // chbSearchLastNameEquals
            // 
            this.chbSearchLastNameEquals.Location = new System.Drawing.Point(73, 56);
            this.chbSearchLastNameEquals.Name = "chbSearchLastNameEquals";
            this.chbSearchLastNameEquals.Size = new System.Drawing.Size(112, 20);
            this.chbSearchLastNameEquals.TabIndex = 8;
            this.chbSearchLastNameEquals.Values.Text = "Слово целиком";
            this.chbSearchLastNameEquals.CheckedChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // lblSearchLastName
            // 
            this.lblSearchLastName.Location = new System.Drawing.Point(3, 56);
            this.lblSearchLastName.Name = "lblSearchLastName";
            this.lblSearchLastName.Size = new System.Drawing.Size(64, 20);
            this.lblSearchLastName.TabIndex = 7;
            this.lblSearchLastName.Values.Text = "Фамилия:";
            // 
            // tbSearchInitials
            // 
            this.tbSearchInitials.Location = new System.Drawing.Point(191, 30);
            this.tbSearchInitials.Name = "tbSearchInitials";
            this.tbSearchInitials.Size = new System.Drawing.Size(121, 20);
            this.tbSearchInitials.TabIndex = 6;
            this.tbSearchInitials.TextChanged += new System.EventHandler(this.CardsParameterChanged);
            this.tbSearchInitials.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // chbSearchInitialsEquals
            // 
            this.chbSearchInitialsEquals.Location = new System.Drawing.Point(73, 30);
            this.chbSearchInitialsEquals.Name = "chbSearchInitialsEquals";
            this.chbSearchInitialsEquals.Size = new System.Drawing.Size(112, 20);
            this.chbSearchInitialsEquals.TabIndex = 5;
            this.chbSearchInitialsEquals.Values.Text = "Слово целиком";
            this.chbSearchInitialsEquals.CheckedChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // lblSearchInitials
            // 
            this.lblSearchInitials.Location = new System.Drawing.Point(3, 30);
            this.lblSearchInitials.Name = "lblSearchInitials";
            this.lblSearchInitials.Size = new System.Drawing.Size(66, 20);
            this.lblSearchInitials.TabIndex = 4;
            this.lblSearchInitials.Values.Text = "Отчество:";
            // 
            // tbSearchFirstName
            // 
            this.tbSearchFirstName.Location = new System.Drawing.Point(191, 4);
            this.tbSearchFirstName.Name = "tbSearchFirstName";
            this.tbSearchFirstName.Size = new System.Drawing.Size(121, 20);
            this.tbSearchFirstName.TabIndex = 3;
            this.tbSearchFirstName.TextChanged += new System.EventHandler(this.CardsParameterChanged);
            this.tbSearchFirstName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // chbSearchFirstNameEquals
            // 
            this.chbSearchFirstNameEquals.Location = new System.Drawing.Point(73, 4);
            this.chbSearchFirstNameEquals.Name = "chbSearchFirstNameEquals";
            this.chbSearchFirstNameEquals.Size = new System.Drawing.Size(112, 20);
            this.chbSearchFirstNameEquals.TabIndex = 2;
            this.chbSearchFirstNameEquals.Values.Text = "Слово целиком";
            this.chbSearchFirstNameEquals.CheckedChanged += new System.EventHandler(this.CardsParameterChanged);
            // 
            // lblSearchFirstName
            // 
            this.lblSearchFirstName.Location = new System.Drawing.Point(3, 4);
            this.lblSearchFirstName.Name = "lblSearchFirstName";
            this.lblSearchFirstName.Size = new System.Drawing.Size(37, 20);
            this.lblSearchFirstName.TabIndex = 1;
            this.lblSearchFirstName.Values.Text = "Имя:";
            // 
            // tabVisits
            // 
            this.tabVisits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.tabVisits.Controls.Add(this.dgVisits);
            this.tabVisits.Controls.Add(this.pnlVisits);
            this.tabVisits.Location = new System.Drawing.Point(4, 22);
            this.tabVisits.Name = "tabVisits";
            this.tabVisits.Size = new System.Drawing.Size(598, 336);
            this.tabVisits.TabIndex = 1;
            this.tabVisits.Text = "Приемы";
            // 
            // dgVisits
            // 
            this.dgVisits.AllowUserToAddRows = false;
            this.dgVisits.AllowUserToDeleteRows = false;
            this.dgVisits.AllowUserToOrderColumns = true;
            this.dgVisits.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVisits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgVisits.Location = new System.Drawing.Point(0, 84);
            this.dgVisits.MultiSelect = false;
            this.dgVisits.Name = "dgVisits";
            this.dgVisits.ReadOnly = true;
            this.dgVisits.RowHeadersVisible = false;
            this.dgVisits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVisits.Size = new System.Drawing.Size(598, 252);
            this.dgVisits.TabIndex = 5;
            this.dgVisits.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_CellMouseClick);
            // 
            // pnlVisits
            // 
            this.pnlVisits.Controls.Add(this.cmbVisitDate);
            this.pnlVisits.Controls.Add(this.dtVisitDate);
            this.pnlVisits.Controls.Add(this.lblVisitsDate);
            this.pnlVisits.Controls.Add(this.tbVisitCard);
            this.pnlVisits.Controls.Add(this.chbVisitCard);
            this.pnlVisits.Controls.Add(this.lblVisitCard);
            this.pnlVisits.Controls.Add(this.tbVisitDisease);
            this.pnlVisits.Controls.Add(this.chbVisitsDisease);
            this.pnlVisits.Controls.Add(this.lblVisitDisease);
            this.pnlVisits.Controls.Add(this.tbVisitDoctor);
            this.pnlVisits.Controls.Add(this.chbVisitDoctor);
            this.pnlVisits.Controls.Add(this.lblVisitDoctor);
            this.pnlVisits.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlVisits.Location = new System.Drawing.Point(0, 0);
            this.pnlVisits.Name = "pnlVisits";
            this.pnlVisits.Size = new System.Drawing.Size(598, 84);
            this.pnlVisits.TabIndex = 4;
            // 
            // cmbVisitDate
            // 
            this.cmbVisitDate.DropDownWidth = 45;
            this.cmbVisitDate.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.cmbVisitDate.Location = new System.Drawing.Point(414, 2);
            this.cmbVisitDate.Name = "cmbVisitDate";
            this.cmbVisitDate.Size = new System.Drawing.Size(45, 21);
            this.cmbVisitDate.TabIndex = 14;
            this.cmbVisitDate.SelectedIndexChanged += new System.EventHandler(this.VisitsParametersChanged);
            // 
            // dtVisitDate
            // 
            this.dtVisitDate.CalendarTodayDate = new System.DateTime(2011, 2, 27, 0, 0, 0, 0);
            this.dtVisitDate.Location = new System.Drawing.Point(465, 3);
            this.dtVisitDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtVisitDate.Name = "dtVisitDate";
            this.dtVisitDate.Size = new System.Drawing.Size(117, 21);
            this.dtVisitDate.TabIndex = 11;
            this.dtVisitDate.ValueChanged += new System.EventHandler(this.VisitsParametersChanged);
            // 
            // lblVisitsDate
            // 
            this.lblVisitsDate.Location = new System.Drawing.Point(318, 4);
            this.lblVisitsDate.Name = "lblVisitsDate";
            this.lblVisitsDate.Size = new System.Drawing.Size(107, 20);
            this.lblVisitsDate.TabIndex = 10;
            this.lblVisitsDate.Values.Text = "Дата посещения:";
            // 
            // tbVisitCard
            // 
            this.tbVisitCard.Location = new System.Drawing.Point(191, 56);
            this.tbVisitCard.Name = "tbVisitCard";
            this.tbVisitCard.Size = new System.Drawing.Size(121, 20);
            this.tbVisitCard.TabIndex = 9;
            this.tbVisitCard.TextChanged += new System.EventHandler(this.VisitsParametersChanged);
            this.tbVisitCard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // chbVisitCard
            // 
            this.chbVisitCard.Location = new System.Drawing.Point(73, 56);
            this.chbVisitCard.Name = "chbVisitCard";
            this.chbVisitCard.Size = new System.Drawing.Size(112, 20);
            this.chbVisitCard.TabIndex = 8;
            this.chbVisitCard.Values.Text = "Слово целиком";
            // 
            // lblVisitCard
            // 
            this.lblVisitCard.Location = new System.Drawing.Point(3, 56);
            this.lblVisitCard.Name = "lblVisitCard";
            this.lblVisitCard.Size = new System.Drawing.Size(64, 20);
            this.lblVisitCard.TabIndex = 7;
            this.lblVisitCard.Values.Text = "Фамилия:";
            // 
            // tbVisitDisease
            // 
            this.tbVisitDisease.Location = new System.Drawing.Point(191, 30);
            this.tbVisitDisease.Name = "tbVisitDisease";
            this.tbVisitDisease.Size = new System.Drawing.Size(121, 20);
            this.tbVisitDisease.TabIndex = 6;
            this.tbVisitDisease.TextChanged += new System.EventHandler(this.VisitsParametersChanged);
            // 
            // chbVisitsDisease
            // 
            this.chbVisitsDisease.Location = new System.Drawing.Point(73, 30);
            this.chbVisitsDisease.Name = "chbVisitsDisease";
            this.chbVisitsDisease.Size = new System.Drawing.Size(112, 20);
            this.chbVisitsDisease.TabIndex = 5;
            this.chbVisitsDisease.Values.Text = "Слово целиком";
            // 
            // lblVisitDisease
            // 
            this.lblVisitDisease.Location = new System.Drawing.Point(3, 30);
            this.lblVisitDisease.Name = "lblVisitDisease";
            this.lblVisitDisease.Size = new System.Drawing.Size(60, 20);
            this.lblVisitDisease.TabIndex = 4;
            this.lblVisitDisease.Values.Text = "Диагноз:";
            // 
            // tbVisitDoctor
            // 
            this.tbVisitDoctor.Location = new System.Drawing.Point(191, 4);
            this.tbVisitDoctor.Name = "tbVisitDoctor";
            this.tbVisitDoctor.Size = new System.Drawing.Size(121, 20);
            this.tbVisitDoctor.TabIndex = 3;
            this.tbVisitDoctor.TextChanged += new System.EventHandler(this.VisitsParametersChanged);
            this.tbVisitDoctor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_KeyUp);
            // 
            // chbVisitDoctor
            // 
            this.chbVisitDoctor.Location = new System.Drawing.Point(73, 4);
            this.chbVisitDoctor.Name = "chbVisitDoctor";
            this.chbVisitDoctor.Size = new System.Drawing.Size(112, 20);
            this.chbVisitDoctor.TabIndex = 2;
            this.chbVisitDoctor.Values.Text = "Слово целиком";
            // 
            // lblVisitDoctor
            // 
            this.lblVisitDoctor.Location = new System.Drawing.Point(3, 4);
            this.lblVisitDoctor.Name = "lblVisitDoctor";
            this.lblVisitDoctor.Size = new System.Drawing.Size(41, 20);
            this.lblVisitDoctor.TabIndex = 1;
            this.lblVisitDoctor.Values.Text = "Врач:";
            // 
            // tabDiseases
            // 
            this.tabDiseases.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.tabDiseases.Controls.Add(this.dgDiseases);
            this.tabDiseases.Controls.Add(this.pnlDiseases);
            this.tabDiseases.Location = new System.Drawing.Point(4, 22);
            this.tabDiseases.Name = "tabDiseases";
            this.tabDiseases.Size = new System.Drawing.Size(598, 336);
            this.tabDiseases.TabIndex = 2;
            this.tabDiseases.Text = "Диагнозы";
            // 
            // dgDiseases
            // 
            this.dgDiseases.AllowUserToAddRows = false;
            this.dgDiseases.AllowUserToDeleteRows = false;
            this.dgDiseases.AllowUserToOrderColumns = true;
            this.dgDiseases.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDiseases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDiseases.Location = new System.Drawing.Point(0, 84);
            this.dgDiseases.MultiSelect = false;
            this.dgDiseases.Name = "dgDiseases";
            this.dgDiseases.ReadOnly = true;
            this.dgDiseases.RowHeadersVisible = false;
            this.dgDiseases.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDiseases.Size = new System.Drawing.Size(598, 252);
            this.dgDiseases.TabIndex = 9;
            this.dgDiseases.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_CellMouseClick);
            // 
            // pnlDiseases
            // 
            this.pnlDiseases.Controls.Add(this.tbDiseaseCathegory);
            this.pnlDiseases.Controls.Add(this.chbDiseaseCathegory);
            this.pnlDiseases.Controls.Add(this.lblDiseaseCathegory);
            this.pnlDiseases.Controls.Add(this.tbDiseaseName);
            this.pnlDiseases.Controls.Add(this.chbDiseaseName);
            this.pnlDiseases.Controls.Add(this.lblDiseaseName);
            this.pnlDiseases.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDiseases.Location = new System.Drawing.Point(0, 0);
            this.pnlDiseases.Name = "pnlDiseases";
            this.pnlDiseases.Size = new System.Drawing.Size(598, 84);
            this.pnlDiseases.TabIndex = 8;
            // 
            // tbDiseaseCathegory
            // 
            this.tbDiseaseCathegory.Location = new System.Drawing.Point(191, 30);
            this.tbDiseaseCathegory.Name = "tbDiseaseCathegory";
            this.tbDiseaseCathegory.Size = new System.Drawing.Size(121, 20);
            this.tbDiseaseCathegory.TabIndex = 6;
            this.tbDiseaseCathegory.TextChanged += new System.EventHandler(this.DiseasesParametersChanged);
            // 
            // chbDiseaseCathegory
            // 
            this.chbDiseaseCathegory.Location = new System.Drawing.Point(73, 30);
            this.chbDiseaseCathegory.Name = "chbDiseaseCathegory";
            this.chbDiseaseCathegory.Size = new System.Drawing.Size(112, 20);
            this.chbDiseaseCathegory.TabIndex = 5;
            this.chbDiseaseCathegory.Values.Text = "Слово целиком";
            this.chbDiseaseCathegory.CheckedChanged += new System.EventHandler(this.DiseasesParametersChanged);
            // 
            // lblDiseaseCathegory
            // 
            this.lblDiseaseCathegory.Location = new System.Drawing.Point(3, 30);
            this.lblDiseaseCathegory.Name = "lblDiseaseCathegory";
            this.lblDiseaseCathegory.Size = new System.Drawing.Size(71, 20);
            this.lblDiseaseCathegory.TabIndex = 4;
            this.lblDiseaseCathegory.Values.Text = "Категория:";
            // 
            // tbDiseaseName
            // 
            this.tbDiseaseName.Location = new System.Drawing.Point(191, 4);
            this.tbDiseaseName.Name = "tbDiseaseName";
            this.tbDiseaseName.Size = new System.Drawing.Size(121, 20);
            this.tbDiseaseName.TabIndex = 3;
            this.tbDiseaseName.TextChanged += new System.EventHandler(this.DiseasesParametersChanged);
            // 
            // chbDiseaseName
            // 
            this.chbDiseaseName.Location = new System.Drawing.Point(73, 4);
            this.chbDiseaseName.Name = "chbDiseaseName";
            this.chbDiseaseName.Size = new System.Drawing.Size(112, 20);
            this.chbDiseaseName.TabIndex = 2;
            this.chbDiseaseName.Values.Text = "Слово целиком";
            this.chbDiseaseName.CheckedChanged += new System.EventHandler(this.DiseasesParametersChanged);
            // 
            // lblDiseaseName
            // 
            this.lblDiseaseName.Location = new System.Drawing.Point(3, 4);
            this.lblDiseaseName.Name = "lblDiseaseName";
            this.lblDiseaseName.Size = new System.Drawing.Size(67, 20);
            this.lblDiseaseName.TabIndex = 1;
            this.lblDiseaseName.Values.Text = "Название:";
            // 
            // tabServices
            // 
            this.tabServices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.tabServices.Controls.Add(this.dgServices);
            this.tabServices.Controls.Add(this.pnlServices);
            this.tabServices.Location = new System.Drawing.Point(4, 22);
            this.tabServices.Name = "tabServices";
            this.tabServices.Size = new System.Drawing.Size(598, 336);
            this.tabServices.TabIndex = 3;
            this.tabServices.Text = "Услуги";
            // 
            // dgServices
            // 
            this.dgServices.AllowUserToAddRows = false;
            this.dgServices.AllowUserToDeleteRows = false;
            this.dgServices.AllowUserToOrderColumns = true;
            this.dgServices.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgServices.Location = new System.Drawing.Point(0, 84);
            this.dgServices.MultiSelect = false;
            this.dgServices.Name = "dgServices";
            this.dgServices.ReadOnly = true;
            this.dgServices.RowHeadersVisible = false;
            this.dgServices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgServices.Size = new System.Drawing.Size(598, 252);
            this.dgServices.TabIndex = 7;
            this.dgServices.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgServices_CellContentClick);
            this.dgServices.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_CellMouseClick);
            // 
            // pnlServices
            // 
            this.pnlServices.Controls.Add(this.cmbServicesPrice);
            this.pnlServices.Controls.Add(this.tbServicesPrice);
            this.pnlServices.Controls.Add(this.lblServicesPrice);
            this.pnlServices.Controls.Add(this.tbServicesName);
            this.pnlServices.Controls.Add(this.chbServicesName);
            this.pnlServices.Controls.Add(this.lblServicesName);
            this.pnlServices.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServices.Location = new System.Drawing.Point(0, 0);
            this.pnlServices.Name = "pnlServices";
            this.pnlServices.Size = new System.Drawing.Size(598, 84);
            this.pnlServices.TabIndex = 6;
            // 
            // cmbServicesPrice
            // 
            this.cmbServicesPrice.DropDownWidth = 45;
            this.cmbServicesPrice.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.cmbServicesPrice.Location = new System.Drawing.Point(73, 30);
            this.cmbServicesPrice.Name = "cmbServicesPrice";
            this.cmbServicesPrice.Size = new System.Drawing.Size(45, 21);
            this.cmbServicesPrice.TabIndex = 15;
            this.cmbServicesPrice.SelectedIndexChanged += new System.EventHandler(this.ServicesParametersChanged);
            // 
            // tbServicesPrice
            // 
            this.tbServicesPrice.Location = new System.Drawing.Point(191, 30);
            this.tbServicesPrice.Name = "tbServicesPrice";
            this.tbServicesPrice.Size = new System.Drawing.Size(121, 20);
            this.tbServicesPrice.TabIndex = 6;
            this.tbServicesPrice.TextChanged += new System.EventHandler(this.ServicesParametersChanged);
            this.tbServicesPrice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbServicesPrice_KeyUp);
            // 
            // lblServicesPrice
            // 
            this.lblServicesPrice.Location = new System.Drawing.Point(3, 30);
            this.lblServicesPrice.Name = "lblServicesPrice";
            this.lblServicesPrice.Size = new System.Drawing.Size(42, 20);
            this.lblServicesPrice.TabIndex = 4;
            this.lblServicesPrice.Values.Text = "Цена:";
            // 
            // tbServicesName
            // 
            this.tbServicesName.Location = new System.Drawing.Point(191, 4);
            this.tbServicesName.Name = "tbServicesName";
            this.tbServicesName.Size = new System.Drawing.Size(121, 20);
            this.tbServicesName.TabIndex = 3;
            this.tbServicesName.TextChanged += new System.EventHandler(this.ServicesParametersChanged);
            // 
            // chbServicesName
            // 
            this.chbServicesName.Location = new System.Drawing.Point(73, 4);
            this.chbServicesName.Name = "chbServicesName";
            this.chbServicesName.Size = new System.Drawing.Size(112, 20);
            this.chbServicesName.TabIndex = 2;
            this.chbServicesName.Values.Text = "Слово целиком";
            this.chbServicesName.CheckedChanged += new System.EventHandler(this.ServicesParametersChanged);
            // 
            // lblServicesName
            // 
            this.lblServicesName.Location = new System.Drawing.Point(3, 4);
            this.lblServicesName.Name = "lblServicesName";
            this.lblServicesName.Size = new System.Drawing.Size(67, 20);
            this.lblServicesName.TabIndex = 1;
            this.lblServicesName.Values.Text = "Название:";
            // 
            // tabDoctors
            // 
            this.tabDoctors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.tabDoctors.Controls.Add(this.dgDoctors);
            this.tabDoctors.Controls.Add(this.pnlDoctors);
            this.tabDoctors.Location = new System.Drawing.Point(4, 22);
            this.tabDoctors.Name = "tabDoctors";
            this.tabDoctors.Size = new System.Drawing.Size(598, 336);
            this.tabDoctors.TabIndex = 3;
            this.tabDoctors.Text = "Врачи";
            // 
            // dgDoctors
            // 
            this.dgDoctors.AllowUserToAddRows = false;
            this.dgDoctors.AllowUserToDeleteRows = false;
            this.dgDoctors.AllowUserToOrderColumns = true;
            this.dgDoctors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDoctors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDoctors.Location = new System.Drawing.Point(0, 84);
            this.dgDoctors.MultiSelect = false;
            this.dgDoctors.Name = "dgDoctors";
            this.dgDoctors.ReadOnly = true;
            this.dgDoctors.RowHeadersVisible = false;
            this.dgDoctors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDoctors.Size = new System.Drawing.Size(598, 252);
            this.dgDoctors.TabIndex = 7;
            this.dgDoctors.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_CellMouseClick);
            // 
            // pnlDoctors
            // 
            this.pnlDoctors.Controls.Add(this.tbDoctorsPost);
            this.pnlDoctors.Controls.Add(this.chbDoctorsPost);
            this.pnlDoctors.Controls.Add(this.lblDoctorPost);
            this.pnlDoctors.Controls.Add(this.tbDoctorsName);
            this.pnlDoctors.Controls.Add(this.chbDoctorsName);
            this.pnlDoctors.Controls.Add(this.lblDoctorsName);
            this.pnlDoctors.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDoctors.Location = new System.Drawing.Point(0, 0);
            this.pnlDoctors.Name = "pnlDoctors";
            this.pnlDoctors.Size = new System.Drawing.Size(598, 84);
            this.pnlDoctors.TabIndex = 6;
            // 
            // tbDoctorsPost
            // 
            this.tbDoctorsPost.Location = new System.Drawing.Point(191, 30);
            this.tbDoctorsPost.Name = "tbDoctorsPost";
            this.tbDoctorsPost.Size = new System.Drawing.Size(121, 20);
            this.tbDoctorsPost.TabIndex = 6;
            this.tbDoctorsPost.TextChanged += new System.EventHandler(this.DoctorsParametersChanged);
            // 
            // chbDoctorsPost
            // 
            this.chbDoctorsPost.Location = new System.Drawing.Point(73, 30);
            this.chbDoctorsPost.Name = "chbDoctorsPost";
            this.chbDoctorsPost.Size = new System.Drawing.Size(112, 20);
            this.chbDoctorsPost.TabIndex = 5;
            this.chbDoctorsPost.Values.Text = "Слово целиком";
            this.chbDoctorsPost.CheckedChanged += new System.EventHandler(this.DoctorsParametersChanged);
            // 
            // lblDoctorPost
            // 
            this.lblDoctorPost.Location = new System.Drawing.Point(3, 30);
            this.lblDoctorPost.Name = "lblDoctorPost";
            this.lblDoctorPost.Size = new System.Drawing.Size(76, 20);
            this.lblDoctorPost.TabIndex = 4;
            this.lblDoctorPost.Values.Text = "Должность:";
            // 
            // tbDoctorsName
            // 
            this.tbDoctorsName.Location = new System.Drawing.Point(191, 4);
            this.tbDoctorsName.Name = "tbDoctorsName";
            this.tbDoctorsName.Size = new System.Drawing.Size(121, 20);
            this.tbDoctorsName.TabIndex = 3;
            this.tbDoctorsName.TextChanged += new System.EventHandler(this.DoctorsParametersChanged);
            // 
            // chbDoctorsName
            // 
            this.chbDoctorsName.Location = new System.Drawing.Point(73, 4);
            this.chbDoctorsName.Name = "chbDoctorsName";
            this.chbDoctorsName.Size = new System.Drawing.Size(112, 20);
            this.chbDoctorsName.TabIndex = 2;
            this.chbDoctorsName.Values.Text = "Слово целиком";
            this.chbDoctorsName.CheckedChanged += new System.EventHandler(this.DoctorsParametersChanged);
            // 
            // lblDoctorsName
            // 
            this.lblDoctorsName.Location = new System.Drawing.Point(3, 4);
            this.lblDoctorsName.Name = "lblDoctorsName";
            this.lblDoctorsName.Size = new System.Drawing.Size(64, 20);
            this.lblDoctorsName.TabIndex = 1;
            this.lblDoctorsName.Values.Text = "Фамилия:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(206)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(706, 362);
            this.Controls.Add(this.tabMainNavi);
            this.Controls.Add(this.pnlControls);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmMain";
            this.Text = "Aurora Medical";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlControls)).EndInit();
            this.pnlControls.ResumeLayout(false);
            this.tabMainNavi.ResumeLayout(false);
            this.tabCards.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCards)).EndInit();
            this.pnlCards.ResumeLayout(false);
            this.pnlCards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSearchDateSwitch)).EndInit();
            this.tabVisits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlVisits)).EndInit();
            this.pnlVisits.ResumeLayout(false);
            this.pnlVisits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVisitDate)).EndInit();
            this.tabDiseases.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDiseases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDiseases)).EndInit();
            this.pnlDiseases.ResumeLayout(false);
            this.pnlDiseases.PerformLayout();
            this.tabServices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgServices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlServices)).EndInit();
            this.pnlServices.ResumeLayout(false);
            this.pnlServices.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServicesPrice)).EndInit();
            this.tabDoctors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDoctors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDoctors)).EndInit();
            this.pnlDoctors.ResumeLayout(false);
            this.pnlDoctors.PerformLayout();
            this.ResumeLayout(false);

        }

        private ComponentFactory.Krypton.Toolkit.KryptonManager mgrMainKryptonManager;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlControls;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnAdd;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenu cmEditDelete;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems cmItems;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem mnuEdit;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem mnuDelete;
        private System.Windows.Forms.TabControl tabMainNavi;
        private System.Windows.Forms.TabPage tabCards;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlCards;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbSearchDateSwitch;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbSearchAddress;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblSearchAddress;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dtSearchBirth;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblSearchBirth;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbSearchLastName;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbSearchLastNameEquals;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblSearchLastName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbSearchInitials;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbSearchInitialsEquals;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblSearchInitials;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbSearchFirstName;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbSearchFirstNameEquals;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblSearchFirstName;
        private System.Windows.Forms.TabPage tabVisits;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlVisits;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbVisitDate;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dtVisitDate;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitsDate;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbVisitCard;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbVisitCard;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitCard;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbVisitDisease;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbVisitsDisease;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitDisease;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbVisitDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbVisitDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblVisitDoctor;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnExit;
        private System.Windows.Forms.TabPage tabDiseases;
        private System.Windows.Forms.TabPage tabServices;
        private System.Windows.Forms.TabPage tabDoctors;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlServices;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbServicesPrice;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServicesPrice;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbServicesName;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbServicesName;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblServicesName;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlDoctors;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDoctorsPost;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbDoctorsPost;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDoctorPost;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDoctorsName;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbDoctorsName;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDoctorsName;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel pnlDiseases;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiseaseName;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbDiseaseName;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiseaseName;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnVisit;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgCards;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgVisits;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgDiseases;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgServices;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgDoctors;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbServicesPrice;
        private ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem mnuVisits;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbDiseaseCathegory;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox chbDiseaseCathegory;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblDiseaseCathegory;
    }
}