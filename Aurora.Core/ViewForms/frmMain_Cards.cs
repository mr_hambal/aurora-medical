using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Aurora.Domain;
using Aurora.Infrastructure;

namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        #region Delegates

        public delegate void InvokeDelegate();

        #endregion

        private void FilterCards()
        {
            if (tabMainNavi.SelectedIndex != 0)
            {
                return;
            }

            var oldData = (IEnumerable<ClientCard>)_dgDataSource;

            dgCards.DataSource = oldData.Intersection(Filters.FilterString(oldData, "FirstName",
                                                                           tbSearchFirstName.Text,
                                                                           chbSearchFirstNameEquals.Checked)).
                Intersection(Filters.FilterString(oldData, "Initials", tbSearchInitials.Text,
                                                  chbSearchInitialsEquals.Checked)).
                Intersection(Filters.FilterString(oldData, "LastName", tbSearchLastName.Text,
                                                  chbSearchLastNameEquals.Checked)).
                Intersection(Filters.FilterDate(oldData, "BirthDay", dtSearchBirth.Value,
                                                cmbSearchDateSwitch.SelectedIndex)).
                Intersection(Filters.FilterString(oldData, "Address", tbSearchAddress.Text, false));

            InitCardsColumns();
            return;
        }

        /// <summary>
        /// Call filtering by Cards on change some parameters.
        /// </summary>
        /// <param name="sender">Control</param>
        /// <param name="e"></param>
        private void CardsParameterChanged(object sender, EventArgs e)
        {
            try
            {
                BeginInvoke(new InvokeDelegate(FilterCards));
            }
            catch (InvalidOperationException invalidOperationException)
            {
                Messenger.ShowErrorMessage("������!", "���������� ��� ���. " + invalidOperationException.Message);
            }
            return;
        }

        private void InitCardsColumns()
        {
            dgCards.Columns.Clear();
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() {DataPropertyName = "Id", HeaderText = "��"});
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "LastName", HeaderText = "�������" });
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "FirstName", HeaderText = "���" });
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Initials", HeaderText = "��������" });
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "BirthDay", HeaderText = "�������" });
            dgCards.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Address", HeaderText = "�����" });
            return;
        }
        
        private void ShowCardVisits()
        {
            var dgRowSender = (DataGridViewRow)cmEditDelete.Caller;
            var lastName = dgRowSender.Cells["LastName"].Value.ToString();

            tabMainNavi.SelectTab("tabVisits");
            tbVisitCard.Text = lastName;
            mnuVisits.Visible = false;
            return;
        }
    }
}