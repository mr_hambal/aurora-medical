﻿namespace Aurora.Core.ViewForms
{
    partial class frmConnect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.connKryptMan = new ComponentFactory.Krypton.Toolkit.KryptonManager(this.components);
            this.btnConnect = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.gbConnectSource = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.lblConnectSourcePass = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblConnectSourceLogin = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.lblConnectSourceAddress = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.tbConnectSourcePass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbConnectSourceLogin = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.tbConnectSourceAddress = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cmbConnectSource = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.gbConnectCE = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnConnectCE = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblConnectCE = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gbConnectSource)).BeginInit();
            this.gbConnectSource.Panel.SuspendLayout();
            this.gbConnectSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbConnectSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbConnectCE)).BeginInit();
            this.gbConnectCE.Panel.SuspendLayout();
            this.gbConnectCE.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(103, 110);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(100, 25);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Values.Text = "Соединиться";
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // gbConnectSource
            // 
            this.gbConnectSource.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ControlToolTip;
            this.gbConnectSource.Location = new System.Drawing.Point(12, 12);
            this.gbConnectSource.Name = "gbConnectSource";
            // 
            // gbConnectSource.Panel
            // 
            this.gbConnectSource.Panel.Controls.Add(this.lblConnectSourcePass);
            this.gbConnectSource.Panel.Controls.Add(this.btnConnect);
            this.gbConnectSource.Panel.Controls.Add(this.lblConnectSourceLogin);
            this.gbConnectSource.Panel.Controls.Add(this.lblConnectSourceAddress);
            this.gbConnectSource.Panel.Controls.Add(this.tbConnectSourcePass);
            this.gbConnectSource.Panel.Controls.Add(this.tbConnectSourceLogin);
            this.gbConnectSource.Panel.Controls.Add(this.tbConnectSourceAddress);
            this.gbConnectSource.Panel.Controls.Add(this.cmbConnectSource);
            this.gbConnectSource.Size = new System.Drawing.Size(216, 170);
            this.gbConnectSource.TabIndex = 9;
            this.gbConnectSource.Values.Heading = "Работа в сети";
            // 
            // lblConnectSourcePass
            // 
            this.lblConnectSourcePass.Location = new System.Drawing.Point(3, 85);
            this.lblConnectSourcePass.Name = "lblConnectSourcePass";
            this.lblConnectSourcePass.Size = new System.Drawing.Size(53, 20);
            this.lblConnectSourcePass.TabIndex = 14;
            this.lblConnectSourcePass.Values.Text = "Пароль";
            // 
            // lblConnectSourceLogin
            // 
            this.lblConnectSourceLogin.Location = new System.Drawing.Point(3, 58);
            this.lblConnectSourceLogin.Name = "lblConnectSourceLogin";
            this.lblConnectSourceLogin.Size = new System.Drawing.Size(45, 20);
            this.lblConnectSourceLogin.TabIndex = 13;
            this.lblConnectSourceLogin.Values.Text = "Логин";
            // 
            // lblConnectSourceAddress
            // 
            this.lblConnectSourceAddress.Location = new System.Drawing.Point(3, 32);
            this.lblConnectSourceAddress.Name = "lblConnectSourceAddress";
            this.lblConnectSourceAddress.Size = new System.Drawing.Size(94, 20);
            this.lblConnectSourceAddress.TabIndex = 12;
            this.lblConnectSourceAddress.Values.Text = "Адрес сервера";
            // 
            // tbConnectSourcePass
            // 
            this.tbConnectSourcePass.Location = new System.Drawing.Point(103, 84);
            this.tbConnectSourcePass.Name = "tbConnectSourcePass";
            this.tbConnectSourcePass.Size = new System.Drawing.Size(100, 20);
            this.tbConnectSourcePass.TabIndex = 11;
            // 
            // tbConnectSourceLogin
            // 
            this.tbConnectSourceLogin.Location = new System.Drawing.Point(103, 58);
            this.tbConnectSourceLogin.Name = "tbConnectSourceLogin";
            this.tbConnectSourceLogin.Size = new System.Drawing.Size(100, 20);
            this.tbConnectSourceLogin.TabIndex = 10;
            // 
            // tbConnectSourceAddress
            // 
            this.tbConnectSourceAddress.Location = new System.Drawing.Point(103, 32);
            this.tbConnectSourceAddress.Name = "tbConnectSourceAddress";
            this.tbConnectSourceAddress.Size = new System.Drawing.Size(100, 20);
            this.tbConnectSourceAddress.TabIndex = 9;
            // 
            // cmbConnectSource
            // 
            this.cmbConnectSource.DropDownWidth = 180;
            this.cmbConnectSource.Items.AddRange(new object[] {
            "MS SQL Server",
            "MySQL Server"});
            this.cmbConnectSource.Location = new System.Drawing.Point(14, 5);
            this.cmbConnectSource.Name = "cmbConnectSource";
            this.cmbConnectSource.Size = new System.Drawing.Size(189, 21);
            this.cmbConnectSource.TabIndex = 8;
            // 
            // gbConnectCE
            // 
            this.gbConnectCE.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ControlToolTip;
            this.gbConnectCE.Location = new System.Drawing.Point(12, 188);
            this.gbConnectCE.Name = "gbConnectCE";
            // 
            // gbConnectCE.Panel
            // 
            this.gbConnectCE.Panel.Controls.Add(this.btnConnectCE);
            this.gbConnectCE.Panel.Controls.Add(this.lblConnectCE);
            this.gbConnectCE.Size = new System.Drawing.Size(215, 118);
            this.gbConnectCE.TabIndex = 10;
            this.gbConnectCE.Values.Heading = "Портативная версия";
            // 
            // btnConnectCE
            // 
            this.btnConnectCE.Location = new System.Drawing.Point(102, 62);
            this.btnConnectCE.Name = "btnConnectCE";
            this.btnConnectCE.Size = new System.Drawing.Size(100, 25);
            this.btnConnectCE.TabIndex = 1;
            this.btnConnectCE.Values.Text = "Портативно";
            this.btnConnectCE.Click += new System.EventHandler(this.btnConnectCE_Click);
            // 
            // lblConnectCE
            // 
            this.lblConnectCE.Location = new System.Drawing.Point(4, 4);
            this.lblConnectCE.Name = "lblConnectCE";
            this.lblConnectCE.Size = new System.Drawing.Size(206, 52);
            this.lblConnectCE.TabIndex = 0;
            this.lblConnectCE.Values.Text = "Можно продолжить использовать\r\nвстроенную портативную базу \r\nданных.";
            // 
            // frmConnect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 316);
            this.Controls.Add(this.gbConnectCE);
            this.Controls.Add(this.gbConnectSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmConnect";
            this.Text = "База данных";
            this.gbConnectSource.Panel.ResumeLayout(false);
            this.gbConnectSource.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbConnectSource)).EndInit();
            this.gbConnectSource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbConnectSource)).EndInit();
            this.gbConnectCE.Panel.ResumeLayout(false);
            this.gbConnectCE.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbConnectCE)).EndInit();
            this.gbConnectCE.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonManager connKryptMan;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConnect;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox gbConnectSource;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblConnectSourcePass;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblConnectSourceLogin;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblConnectSourceAddress;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbConnectSourcePass;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbConnectSourceLogin;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox tbConnectSourceAddress;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cmbConnectSource;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox gbConnectCE;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConnectCE;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblConnectCE;
    }
}