using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Aurora.Domain;
using Aurora.Infrastructure;

namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        private void FilterDoctors()
        {
            if (tabMainNavi.SelectedIndex != 4)
            {
                return;
            }

            var oldData = (IEnumerable<Doctor>)_dgDataSource;

            dgDoctors.DataSource = oldData.Intersection(Filters.FilterString(oldData, "Name",
                                                                              tbDoctorsName.Text,
                                                                              chbDoctorsName.Checked)).
                                            Intersection(Filters.FilterString(oldData,
                                                                              "Post",
                                                                              tbDoctorsPost.Text,
                                                                              chbDoctorsPost.Checked));
            InitDoctorColumns();
            return;
        }

        /// <summary>
        /// Call filtering by Doctors on change some parameters.
        /// </summary>
        /// <param name="sender">Control</param>
        /// <param name="e"></param>
        private void DoctorsParametersChanged(object sender, EventArgs e)
        {
            try
            {
                BeginInvoke(new InvokeDelegate(FilterDoctors));
            }
            catch (InvalidOperationException invalidOperationException)
            {
                Messenger.ShowErrorMessage("������!", "���������� ��� ���. " + invalidOperationException.Message);
            }
            return;
        }

        private void InitDoctorColumns()
        {
            dgDoctors.Columns.Clear();
            dgDoctors.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Id", HeaderText = "��" });
            dgDoctors.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", HeaderText = "���" });
            dgDoctors.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Post", HeaderText = "���������" });
            dgDoctors.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Info", HeaderText = "����" });
            return;
        }
    }
}