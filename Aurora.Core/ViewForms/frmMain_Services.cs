using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Aurora.Domain;
using Aurora.Infrastructure;
using ComponentFactory.Krypton.Toolkit;

namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        private void FilterServices()
        {
            if (tabMainNavi.SelectedIndex != 3)
            {
                return;
            }

            var oldData = (IEnumerable<Service>)_dgDataSource;
            Single price;

            if (!Single.TryParse(tbServicesPrice.Text, out price))
                price = 0;

            dgServices.DataSource = oldData.Intersection(Filters.FilterString(oldData, "Name",
                                                                              tbServicesName.Text,
                                                                              chbServicesName.Checked)).
                                            Intersection(Filters.FilterPrice(oldData,
                                                                              "Price",
                                                                              price,
                                                                              cmbServicesPrice.SelectedIndex));
            return;
        }

        /// <summary>
        /// Call filtering by Diseases on change some parameters.
        /// </summary>
        /// <param name="sender">Control</param>
        /// <param name="e"></param>
        private void ServicesParametersChanged(object sender, EventArgs e)
        {
            try
            {
                BeginInvoke(new InvokeDelegate(FilterServices));
            }
            catch (InvalidOperationException invalidOperationException)
            {
                Messenger.ShowErrorMessage("������!", "���������� ��� ���. " + invalidOperationException.Message);
            }
            return;
        }

        private void tbServicesPrice_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var textBox = (KryptonTextBox)sender;
                textBox.Text = TextProcessor.PriceChar(textBox.Text);
            }
            catch (InvalidCastException)
            {
                Messenger.ShowErrorMessage("Invalid cast exception", "����������� ������. ���������� � ������������.");
            }
            catch (Exception)
            {
                Messenger.ShowErrorMessage("������ ���������� ����������� ���������",
                                           "���� ��� ���������� ���������. ������������� ���������� ��� ���������� ��������� ���� ��������.");
            }
            return;
        }

        private void ServicesAsDataSource()
        {
            _dgDataSource = ((AuroraEntities)_repository.GetContext()).Services.
                Include("Visits").
                ToList();

            dgServices.DataSource = _dgDataSource;

            dgServices.Columns.Clear();
            dgServices.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Id", HeaderText = "��" });
            dgServices.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", HeaderText = "��������" });
            dgServices.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Price", HeaderText = "����" });
            dgServices.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Info", HeaderText = "����" });
            dgServices.Columns.Add(new DataGridViewButtonColumn() { DataPropertyName = "Visits", HeaderText = "������" });
            return;
        }
        
        private void dgServices_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 4)
            {
                return;
            }
            var id = (Int64) dgServices.Rows[e.RowIndex].Cells[0].Value;

            tabMainNavi.SelectTab(1);

            var dataSource = (IEnumerable<Visit>) _dgDataSource;

            dgVisits.DataSource =
                dataSource.Intersection
                    (
                        (from visit in dataSource
                         from service in visit.Services
                         where service.Id == id
                         select visit).ToList()
                    );
        }

    }

}