using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Aurora.Domain;
using Aurora.Infrastructure;

namespace Aurora.Core.ViewForms
{
    partial class frmMain
    {
        private void FilterDiseases()
        {
            if (tabMainNavi.SelectedIndex != 2)
            {
                return;
            }

            var oldData = (IEnumerable<Disease>)_dgDataSource;

            dgDiseases.DataSource = oldData.Intersection(Filters.FilterString(oldData, "Name",
                                                                              tbDiseaseName.Text,
                                                                              chbDiseaseName.Checked)).
                                            Intersection(Filters.FilterString(oldData, 
                                                                              "Cathegory", 
                                                                              tbDiseaseCathegory.Text,
                                                                              chbDiseaseCathegory.Checked));
            InitDiseaseColumns();
            return;
        }

        /// <summary>
        /// Call filtering by Diseases on change some parameters.
        /// </summary>
        /// <param name="sender">Control</param>
        /// <param name="e"></param>
        private void DiseasesParametersChanged(object sender, EventArgs e)
        {
            try
            {
                BeginInvoke(new InvokeDelegate(FilterDiseases));
            }
            catch (InvalidOperationException invalidOperationException)
            {
                Messenger.ShowErrorMessage("������!", "���������� ��� ���. " + invalidOperationException.Message);
            }
            return;
        }

        private void InitDiseaseColumns()
        {
            dgDiseases.Columns.Clear();
            dgDiseases.Columns.Add(new DataGridViewTextBoxColumn() {DataPropertyName = "Id", HeaderText = "��"});
            dgDiseases.Columns.Add(new DataGridViewTextBoxColumn() {DataPropertyName = "Name", HeaderText = "��������"});
            dgDiseases.Columns.Add(new DataGridViewTextBoxColumn() {DataPropertyName = "Cathegory", HeaderText = "���������"});
            dgDiseases.Columns.Add(new DataGridViewTextBoxColumn() {DataPropertyName = "Info", HeaderText = "����"});
            return;
        }
    }
}