using System;
using System.Collections.Generic;
using System.Linq;
using Aurora.Domain;
using Aurora.Infrastructure;

namespace Aurora.Core
{
    public static class Filters
    {
        public static IEnumerable<T> FilterString<T>(IEnumerable<T> dataSource, 
                                                             String propertyName,
                                                             String text,
                                                             Boolean contains) where T:DomainObject
        {
            if (text.Length <= 0) return dataSource;

            IEnumerable<T> ret = new List<T>();

            try
            {
                if (contains)
                {
                    ret = from item in dataSource
                          where
                              item.GetType().GetProperty(propertyName).GetValue(item, null).ToString() ==
                              text
                          select item;
                }
                else
                {
                    ret = from item in dataSource
                          where
                              item.GetType().GetProperty(propertyName).GetValue(item, null).ToString()
                              .Contains(text)
                          select item;
                }

                return ret;
            }
            catch (Exception exception)
            {
                Messenger.ShowErrorMessage("������ ��� ����������", exception.Message);
                return dataSource;
            }
        }

        public static IEnumerable<T> FilterDate<T>(IEnumerable<T> dataSource, String propertyName,
                                                           DateTime birth, int criteria) where T : DomainObject
        {
            switch (criteria)
            {
                case 0:
                    return
                        dataSource.Return(
                            x =>
                            x.Where(
                                t =>
                                t.GetType().GetProperty(propertyName).GetValue(t, null).ToString() ==
                                birth.Date.ToString()), dataSource);
                case 1:
                    return
                        dataSource.Return(
                            x =>
                            x.Where(
                                t =>
                                DateTime.Parse(t.GetType().GetProperty(propertyName).GetValue(t, null).ToString()) >
                                birth.Date), dataSource);
                case 2:
                   return
                        dataSource.Return(
                            x =>
                            x.Where(
                                t =>
                                DateTime.Parse(t.GetType().GetProperty(propertyName).GetValue(t, null).ToString()) <
                                birth.Date), dataSource);
                default:
                    return dataSource;
            }
        }

        public static IEnumerable<T> FilterPrice<T>(IEnumerable<T> dataSource, String propertyName, Single price, int criteria)
        {
            if (price == 0) return dataSource;

            switch (criteria)
            {
                case 0:
                    return
                        dataSource.Return(
                            x =>
                            x.Where(
                                t =>
                                (Double) t.GetType().GetProperty(propertyName).GetValue(t, null) ==
                                price), dataSource);
                case 1:
                    return
                        dataSource.Return(
                            x =>
                            x.Where(
                                t =>
                                (Double) t.GetType().GetProperty(propertyName).GetValue(t, null) >
                                price), dataSource);
                case 2:
                    return
                         dataSource.Return(
                             x =>
                             x.Where(
                                 t =>
                                 (Double)t.GetType().GetProperty(propertyName).GetValue(t, null) <
                                 price), dataSource);
                default:
                    return dataSource;
            }
        }
    }
}