﻿using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Aurora.Infrastructure.IOC
{
    /// <summary>
    /// Класс-фабрика. Создает конфигурированные и неконфигурированные UnityContainer.
    /// </summary>
    public static class IocFabric
    {
        /// <summary>
        /// Creates pure UnityContainer, not configured by something
        /// </summary>
        /// <returns>UnityContainer</returns>
        public static IUnityContainer CreatePure()
        {
            return new UnityContainer();
        }

        /// <summary>
        /// Congiguring Unity IOC container from XML
        /// </summary>
        /// <returns>IUnityContainer</returns>
        public static IUnityContainer CreateConfigured(string mdlName)
        {
            var unityContainer = new UnityContainer();
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

            section.Configure(unityContainer, mdlName);

            return unityContainer;
        }
    }
}