﻿using Aurora.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Aurora.Tests
{
    /// <summary>
    /// Сводное описание для EntitiesTest
    /// </summary>
    [TestClass]
    public class EntitiesTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        [TestMethod]
        public void TestMethod1()
        {
            var auroraEntity = new AuroraEntities();

            Assert.IsInstanceOfType(auroraEntity, typeof(AuroraEntities));
        }
    }
}
