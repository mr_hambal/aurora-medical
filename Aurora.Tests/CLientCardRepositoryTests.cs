﻿using System;
using System.Linq;
using System.Collections.Generic;
using Aurora.DataAccess;
using Aurora.Domain;
using Aurora.Infrastructure.IOC;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Aurora.Tests
{
    [TestClass]
    public class CLientCardRepositoryTests
    {
        private IEnumerable<ClientCard> _origins;
        private IUnityContainer _container;
        private IRepository _ccrepo;

        [TestInitialize]
        public void TestInitialize()
        {
            _origins = new List<ClientCard>() 
                          {
                            ClientCard.Create("Дмитрий", "Геннадиевич", "Гамбаль", DateTime.Now.Date.AddDays(-50), "Луганск"), 
                            ClientCard.Create("Павел", "Викторович", "Рытиков", DateTime.Now.Date.AddDays(-100), "Луганск"), 
                            ClientCard.Create("Яков", "Аркадиевич", "Скиданенко", DateTime.Now.Date.AddMonths(-200), "Свердловск") 
                          };
            _container = IocFabric.CreateConfigured("workingModel");
            _ccrepo = _container.Resolve<IRepository>();
        }

        [TestMethod]
        public void Test1CreateInstance()
        {
            Assert.IsInstanceOfType(_ccrepo, typeof(Repository));
        }

        [TestMethod]
        public void Test2Insert()
        {
            foreach (var clientCard in _origins)
            {
                Assert.IsTrue(_ccrepo.Insert(clientCard));
            }

        }

        [TestMethod]
        public void Test3Save()
        {
            foreach (var clientCard in _origins)
            {
                _ccrepo.Insert(clientCard);
            }
            _ccrepo.SubmitChanges();

            Assert.AreEqual(4, _ccrepo.GetObjects<ClientCard>().Count);
        }

        [TestMethod]
        public void Test4Get()
        {
            var expected = _origins.First();
            var actual = _ccrepo.GetObject<ClientCard>(2);

            Assert.AreEqual(true, expected.Equals(actual));
        }

        [TestMethod]
        public void Test5Delete()
        {
            for (var id = 2; id < 5; id++)
            {
                _ccrepo.Remove(_ccrepo.GetObject<ClientCard>(id));
            }
            _ccrepo.SubmitChanges();

            Assert.AreEqual(1, _ccrepo.GetObjects<ClientCard>().Count);
        }
    }
}
