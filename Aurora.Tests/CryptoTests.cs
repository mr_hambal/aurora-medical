using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aurora.Infrastructure;
using System.IO;
using System.Linq;

namespace Aurora.Tests
{
    [TestClass]
    public class CryptoTests
    {
        /// <summary>
        /// �������������� ����� ��������� ������ <see cref="T:System.Object"/>.
        /// </summary>
        public CryptoTests()
        {
        }

        [TestMethod]
        public void TestAssignNewKey()
        {
            Crypto.RsaAssignNewKey();
            var list = Directory.EnumerateFiles(@"C:\", "*key.xml");

            Assert.IsTrue(list.Count() != 0);
        }

        [TestMethod]
        public void TestEncrdecr()
        {
            const string srcStr = "New testin' string";
            var decrStr = Crypto.RsaDecryptData(Crypto.RsaEncryptData(srcStr));

            Assert.AreEqual(srcStr, decrStr);
        }
    }
}